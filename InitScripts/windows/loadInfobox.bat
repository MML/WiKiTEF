@ECHO OFF

mongoimport --db "WikiTEF" --collection infobox --type tsv --file "../../input/dbpedia_mapping_based_triples.tsv" --fields Entity1,Relation,Entity2 --mode insert
cmd.exe