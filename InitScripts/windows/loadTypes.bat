@ECHO OFF

mongoimport --db "WikiTEF" --collection types --type tsv --file "../../input/instances_types.tsv" --fields Resource,Constant,InstanceType --mode insert
cmd.exe