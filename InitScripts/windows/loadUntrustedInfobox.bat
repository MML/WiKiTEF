@ECHO OFF

mongoimport --db "WikiTEF" --collection infoboxU --type tsv --file "../../input/dbpedia_untrusted_infobox_triples.tsv" --fields Entity1,Relation,Entity2 --mode insert
cmd.exe