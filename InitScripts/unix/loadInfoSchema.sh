#!/bin/sh

mongoimport --db "WikiTEF" --collection infoSchemas --type tsv --file "../../input/dbpedia_info_schema.tsv" --fields Ontology,Relation,Data --mode insert