---
# <center>WiKiTEF:</center> <center>Estrazione di fatti dalle tabelle di Wikipedia </center>
<center>Lorenzo Ariemma, Marco Cappello, Miriana Mancini </center>

---

## 1. INTRODUZIONE
Negli ultimi anni abbiamo assistito all'affermazione dei **Knowledge Graphs** (KGs) come base di conoscenza utilizzata nei motori di ricerca. L'introduzione dei KGs ha portato a notevoli vantaggi tra i quali la possibilità di realizzare delle "semantic search" (più efficienti rispetto alle ricerche basate su keywords), di estrarre informazioni da una grande varietà di sorgenti e di migliorare la qualità dei risultati ottenuti a fronte di una ricerca. 

Per questo motivo recentemente sono stati realizzati diversi esperimenti volti a creare o aumentare i Knowledge Graphs esistenti. Grande attenzione è stata posta su DBpedia e Yago, due KGs creati in maniera automatica mediante l'estrazione di fatti dagli infobox di Wikipedia, e su Freebase, anch'esso contenente al suo interno tali informazioni. Sebbene il solo utilizzo degli _infobox_ abbia permesso di inserire un numero considerevole di fatti nei KGs appena citati, questi contengono solamente una piccola parte delle informazioni presenti negli articoli di Wikipedia, così molte informazioni contenute nei testi o nelle tabelle non sono presenti nè in DBpedia, nè in Yago, nè in Freebase. 

Un piccolo passo avanti è stato fatto grazie ad un progetto di ricerca realizzato dall'Università Roma Tre che ha portato alla realizzazione di _**Lector**_, un sistema in grado di estrarre nuovi fatti dal testo esterno agli infobox.

### 1.1 Il Problema
Resta però ancora una grande quantità di informazione non sfruttata per espandere i KGs: **le tabelle**. 

In Wikipedia infatti molti articoli presentano delle tabelle, che rappresentano dati aggiuntivi circa l'entità principale dell'articolo e che, grazie alla loro struttura compatta e schematica, permettono spesso di cogliere tali informazioni in maniera molto più intuitiva rispetto al testo. Ciononostante queste informazioni non sono presenti in nessuno dei suddetti KGs e sono rimaste fino ad ora inutilizzate nell'ambito della popolazione dei KGs.

### 1.2 WiKiTEF
La possibilità di sfruttare le informazioni contenute nelle tabelle di Wikipedia è alla base del nostro progetto che ha portato alla realizzazione di _**WiKiTEF -Wikipedia Table Extractor Facts-**_, un sistema per l'estrazione di fatti dalle tabelle di Wikipedia.

L'idea di base parte da quella di Lector e consiste nel cercare prima di capire quale sia la relazione che lega due colonne di una tabella, andandola a cercare in un KG di riferimento, e di etichettare poi gli header delle due colonne con la relazione trovata, così come in Lector venivano etichettate le _frasi_ con le relazioni. Questo ci permetterà in un secondo momento di rilevare nuovi fatti da aggiungere al KG.

<L'idea di base parte da quella di Lector, cioè riuscire a capire quale sia la relazione che lega due colonne di una tabella mediante l'utilizzo delle righe stesse già presenti all'interno del KG, per poi aggiungere le righe mancanti.>

Prima di passare a descrivere più in dettaglio l'approccio utilizzato in WiKiTEF, risulta utile descrivere la struttura delle tabelle presenti negli articoli di Wikipedia e di DBpedia in modo tale da facilitare la successiva comprensione del sistema.

### 1.3 Sorgenti

- In Wikipedia le **tabelle** presentano la struttura seguente: ciascuna tabella è caratterizzata da un insieme di righe e di colonne, ognuna delle quali è descritta da un _header_. Ogni singola cella può contenere diverse tipologie di dati tra cui valori testuali, valori numerici, immagini e riferimenti a singole entità oppure a liste di entità, come è possibile vedere nella figura sottostante. Ciascuna entità è identificata mediante un **wikid**, che riveste un ruolo rilevante in quanto, oltre ad essere un riferimento univoco all'entità, viene utilizzato per comporre il _wikilink_, il link alla pagina Wikipedia dell'entità considerata. Ad esempio "Roma_Tre_University" è il wikid per l'entità associata alla nostra università e viene concatenato a https://en.wikipedia.org/wiki/ per ottenere il link alla pagina Wikipedia che è la seguente https://en.wikipedia.org/wiki/Roma_Tre_University. 
 
<div align="center">
<img src="images/Monumenti di Roma Antica.png" alt="Esempio di Tabella" width="500" />
</div>

- **DBpedia** è il Knowledge Graph di riferimento in WiKiTEF. Tale grafo è composto da un insieme di entità, rappresentanti oggetti del mondo reale ed appartenenti ad un certo tipo, e da un insieme di relazioni che collegano tali entità. In una visualizzazione grafica si può immaginare che le entità siano i nodi di un grafo mentre le relazioni gli archi che le collegano. In questo modo una tripla diventa un **fatto**, il concetto di base su cui si fonda il KG. Un fatto costituisce quindi un'istanza di relazione, ovvero una tripla (entità1, relazione, entità2). Per esempio il seguente fatto (Allan_Dwan, birthPlace, Toronto) rappresenta il legame tra l'entità "Alan_Dwan" e l'entità "Toronto" mediante la relazione "birthPlace", indicante quindi che Alan Dwan è nato a Toronto. In particolare, DBpedia è stata creata a partire dagli **infobox** di Wikipedia e questo fa sì che nel grafo sia presente un nodo per ciascuna entità di Wikipedia per la quale viene fornito un infobox.

E' importante a questo punto sottolineare come esista un legame forte tra le entità contenute nelle tabelle di Wikipedia e le entità che costituiscono i nodi del grafo. Questo legame è testimoniato dal fatto che DBpedia utilizza come identificativo per le sue entità proprio il **wikid** di Wikipedia. Riprendendo l'esempio precedente, abbiamo infatti che in DBpedia la nostra università è referenziata con il wikid _Roma_Tre_University_, come è possibile vedere al seguente link [http://dbpedia.org/resource/Roma_Tre_University](http://dbpedia.org/resource/Roma_Tre_University).
Pertanto, una coppia di entità presenti in una tabella di Wikipedia possono essere legate da una relazione e quindi costituire un fatto già esistente in DBpedia oppure un nuovo fatto da rilevare ed aggiungere al grafo.



## 2. APPROCCIO 

L'approccio utilizzato in WiKiTEF per estrarre nuovi fatti con cui espandere il KG DBpedia si sviluppa in **cinque passi** consecutivi:

1. Un primo passo di base, denominato **_Step0_**, nel quale si ripuliscono le tabelle di Wikipedia da campi non utili e si creano a partire da esse tutte le coppie di colonne (i.e. **_bicolumn_**) su cui WiKiTEF dovrà lavorare negli step successivi;
2. Un secondo passo, denominato **_Step1_**, nel quale, per ogni riga di ogni bicolumn creata, si ricercano in DBpedia le relazioni che collegano le sue entità, e se esistono le si associano alla riga stessa. Inoltre si associa il tipo ad ogni entità, in modo tale da ritrovarlo al momento delle verifiche realizzate negli step 3 e 4.
3. Un terzo passo, lo **_Step2_**, in cui per ogni _bicolumn_ creata si cercano una o più relazioni che risultino associate ad un numero di righe tali da superare una determinata soglia. Per ciascuna relazione r individuata vengono poi aggiunte al grafo le triple (s, r, o) per le quali le entità s e o non sono già linkate da r. Inoltre si etichetta la coppia di header della bicolumn con tale relazione.
4. Due ulteriori passi, lo **_Step3_** e lo **_Step4_**, nei quali, per trovare ulteriori nuovi fatti, si riprocessano le bicolumn per le quali il grafo non ci aveva permesso di trovare con certezza la relazione ad essa associata, utilizzando questa volta gli header (ovvero partendo dalle relazioni associate nello step precedente alla coppia di header della bicolumn) ed effettuando dei controlli sui tipi di entità e relazioni.

Una **domanda** che è lecito porsi fin da subito prima di passare a descrivere in dettaglio ciascuno di questi passi è: _"perché individuare più relazioni che rappresentino una bicolumn anzichè una sola?"_.
La risposta risiede nel fatto che ad ogni riga di una bicolumn possono essere associate più relazioni nello step1, pertanto è corretto ammettere la possibilità di selezionare **più relazioni per identificare una bicolumn**, altrimenti scegliendo sempre una sola relazione limiteremmo il numero di fatti inseriti, inserendo in DBpedia solo una parte dei fatti che è realmente possibile inserire, mentre il nostro **scopo** è quello di massimizzarli.


###2.1 Step 0 : Creazione delle Bicolumn
In questo step si lavora su un dump di **1652771** tabelle del sito inglese di Wikipedia in formato JSON, in cui il wikid delle entità prende il nome di **surfaceLink** ed ha associato un tipo che può essere _EXTERNAL_ (link ad entità esterne a Wikipedia), _INTERNAL_RED_ (link a entità di Wikipedia la cui pagina è ancora in costruzione) ed _INTERNAL_ (link ad entità la cui pagina è completa).

#### Creazione delle Bicolumn 
poiché come anticipato l'intento è quello di trovare una relazione per ogni coppia di colonne in ciascuna tabella, la prima operazione da eseguire è proprio quella di creare per ogni tabella l'insieme delle sue **Bicolumn**. Per fare ciò sono state valutate due alternative:

1. la prima prevedeva di individuare all'interno della tabella una colonna principale e poi creare una bicolumn per ogni combinazione di questa colonna con le altre.
2. la seconda prevede invece di creare una bicolumn per ogni possibile combinazione di colonne, senza però creare le bicolumn relative a coppie di colonne già accoppiate ma con ordine inverso.

Si è scelto di utilizzare la seconda strategia in modo da non perdere alcuna informazione e da ottenere un'efficacia maggiore nello step successivo, poiché creando tutte le possibili combinazioni di colonne, se una coppia di colonne è effettivamente legata mediante una relazione del KG questa potrà essere rilevata. Nel caso della prima strategia invece l'utilizzo di un'euristica per la scelta della colonna principale potrebbe portare a problemi in caso di non individuazione della reale colonna principale, non permettendo di rilevare una relazione esistente tra due colonne semplicemente perché quella combinazione di colonne non è stata presa in considerazione. 

#### Pulizia delle righe nelle Bicolumn
Durante la creazione delle bicolumn sono state eseguite delle operazioni ulteriori sulle righe di ciascuna bicolumn, poiché, essendo il nostro scopo quello di individuare delle _corrispondenze tra le coppie di entità linkate in DBpedia e quelle presenti in ciascuna riga di una bicolumn_, dobbiamo essere certi che in ciascuna delle due celle di ogni riga ci sia un riferimento ad una singola entità annotata con il suo wikid (o surfaceLink).
Pertanto si è proceduto a:

1. eliminare dalle bicolumn tutte quelle righe le cui celle non contenevano entrambe un _wikid_. Questo ci ha garantito la possibilità di ripulire le bicolumn da tutte quelle informazioni inutili relative a valori numerici, testuali o immagini, che, essendo privi di wikid, non risultano di nostro interesse in quanto non fanno riferimento ad alcuna entità in DBpedia;
2. eliminare le righe contenenti wikid di tipo _EXTERNAL_, in modo da mantenere solo ed esclusivamente le entità della tabella che posso essere ritrovate in DBpedia;
3. dividere su più righe quelle celle contenenti non un singolo wikid, ma una lista di wikid. Si è così provveduto a creare una nuova riga della bicolumn per ogni wikid della lista contenuta nella cella e replicando eventualmente il valore dell'altra cella.

Inoltre si è deciso di prendere in considerazione solamente le bicolumn con almeno tre righe, poiché si ritiene che un numero inferiore di righe non permetta di individuare una relazione che leghi due colonne, poiché sarebbe semplicemente una relazione che lega una coppia di entità.

---
**Il processo appena illustrato è sintetizzato nelle due figure seguenti:**
<div align="center">
<img src="images/creazione_bicolumn.png" alt="Creazione delle Bicolumn a partire dalla tabella originale" width="600" />
</div>

<div align="center">
<img src="images/pulizia_bicolumn.png" alt="Pulizia delle bicolumn generate" width="600" />
</div>

---

Facciamo ora alcuni esempi per capire ancora meglio cosa accade in questo passo iniziale del nostro approccio.

**Esempio 1:** Segue un primo esempio di tabella che non genera nessuna BiColumn poiché contiene solo valori numerici e nessuna entità.
<div align="center">
<img src="images/table_no_bicolumn.png" alt="Esempio di Tabella senza BiColumn" width="400" />
</div>
**Figura:** Tabella dalla pagina Wikipedia https://en.wikipedia.org/wiki/Vitruvius_(crater). 

**Esempio 2:** Consideriamo ora invece la tabella relativa ai vincitori del Premio Paganini. Tale tabella è composta da tre colonne rispettivamente con headers: _Year_, _Winner_ and _Country_. Da essa vengono prima generate 3 bicolumn (Year, Winner), (Year, Country) e (Winner, Country), ma andando poi ad eliminare tutte le righe in cui almeno una cella non contiene wikid, di fatto dalle prime due bicolumn non sopravvive nemmeno una riga (considerato che la prima colonna contiene tutti valori numerici), pertanto solo l'ultima Bicolumn (Winner, Country) verrà effettivamente memorizzata e passata in input allo step successivo. Inoltre da questa bicolumn non saranno eliminate righe, poiché ciascuna riga ha sempre una coppia di entità identificate da wikid di tipo INTERNAL (entità con link in blu) o di tipo INTERNAL_RED (entità con link rosso).

<div align="center">
<img src="images/table Paganini_Competition.png" alt="Esempio di Tabella con una BiColumn" width="400" />
</div>
**Figura:** una porzione della tabella _Winners_ dalla pagina https://en.wikipedia.org/wiki/Paganini_Competition.


###2.2 Step1 : Ricerca sul grafo delle relazioni da associare alle righe

Il grafo di DBpedia viene fornito in input come un insieme di 15404324 triple con la seguente struttura:

(http://dbpedia.org/resource/Abraham_Lincoln, http://dbpedia.org/ontology/spouse, http://dbpedia.org/resource/Mary_Todd_Lincoln)

Questa tripla ci indica che le entità identificate dai **wikid** Abraham_Lincoln e Mary_Todd_Lincoln sono linkate nel grafo mediante la relazione _spouse_. Significativo è l'ordine delle due entità che indicano rispettivamente che Abraham_Lincoln è il soggetto della tripla, mentre Mary_Todd_Lincoln ne è l'oggetto. Tale fatto è importante da sottolineare, poiché in DBpedia le relazioni non sempre hanno una corrispondente relazione inversa, e ciò implica che potrebbe non esistere la relazione _husband_ e quindi nemmeno la tripla 
	
(http://dbpedia.org/resource/Mary_Todd_Lincoln, http://dbpedia.org/ontology/husband, http://dbpedia.org/resource/Abraham_Lincoln).

#### Ricerca delle righe nel grafo 

Poiché ciascuna **riga** di una bicolumn rappresenta per noi una **coppia di potenziali entità linkate in DBpedia**, l'obiettivo che ci si pone di perseguire in questo step è proprio quello di verificare per ogni riga se esista una tripla corrispondente nel grafo, e in caso affermativo, di associare la relazione trovata alla riga.

Per fare ciò si va ad interrogare il grafo mediante due ricerche mirate: prima si ricerca una tripla che abbia come soggetto il wikid della prima entità della riga e come oggetto il wikid della seconda entità e poi una tripla con le due entità invertite. Questa duplice ricerca è necessaria, poiché, in base a quanto suddetto, non possiamo sapere a priori quale tra le due entità della riga ricopra il ruolo di soggetto o di oggetto nella tripla.
Una volta individuata sul grafo la tripla o le triple corrispondenti alla riga considerata, in primo luogo si ricava da ciascuna tripla la relazione da associare alla riga ed in secondo luogo si marca opportunamente ciascuna relazione con il suo schema (tipo soggetto e tipo oggetto) e con il verso con cui la ricerca ha avuto esito positivo, in modo da poter ricordare quale entità della riga sia il soggetto e quale l'oggetto della relazione. Un'altra informazione che viene aggiunta alla riga sono i **tipi** delle sue entità, in quanto tali dati saranno utili negli step 3 e 4 per effettuare dei controlli di coerenza tra i tipi delle entità ed i tipi presenti negli schemi delle relazioni.
Si ripete poi questo processo per ogni riga di ogni bicolumn ottenuta dallo Step0.

Al termine di questo passo ogni bicolumn risulterà corredata dalle informazioni relative alle relazioni (e al loro schema) associate alle proprie righe, che da qui indicheremo come **relazioni candidate**. 
In questa fase, di fatto, viene preparato il terreno di lavoro per gli step successivi. Pertanto nessuna bicolumn risulta ancora etichettata con una relazione che riesca ad identificarla. Questa operazione sarà realizzata infatti nello step successivo.


###2.3 Step2  : Estrazione di nuovi fatti utilizzando il grafo
In questo step ci si pongono due obiettivi ben precisi: il primo è quello di **espandere DBpedia con nuovi fatti estratti dalle bicolumn** (ove possibile), mentre il secondo consiste nel riuscire ad **associare alle relazioni di DBpedia delle coppie di header che possano rappresentarle**.

Al fine di perseguire tali obiettivi, si itera su tutte le bicolonne ottenuta dallo Step1 e per ciascuna di esse si realizzano le seguenti operazioni:

1. scelta delle _**relazioni rappresentative** per la bicolumn_, tra tutte quelle identificate nello Step1 (i.e. relazioni candidate);
_Solo se il grafo ci permette di trovare con certezza una o più di queste **relazioni**:_
2. aggiunta nel grafo di nuovi fatti per ogni relazione trovata;
3. associazione alle relazioni individuate della coppia di header della bicolumn.	

Vediamo ora di spiegare il tutto con un maggiore livello di dettaglio. 

#### Scelta delle relazioni
Consideriamo una singola bicolonna (bicolumn) per la quale vogliamo trovare almeno una relazione che la identifichi e dalla quale vogliamo estrarre nuovi fatti.
La ricerca di una tale relazione si basa sull'utilizzo di una **soglia** e consiste semplicemente nel _cercare una o più relazioni tra quelle candidate che siano associate ad un numero di righe della bicolumn che in percentuale superi o eguagli il valore della soglia_. Pertanto il **criterio** utilizzato per la scelta delle relazioni da associare alla bicolumn è quindi un criterio prettamente **statistico** che non effettua alcun controllo sulla coerenza tra i tipi delle entità nelle righe e lo schema delle relazioni.
In particolare, questa ricerca viene realizzata assegnando ad ogni relazione candidata un **contatore** che indichi il numero di righe della bicolumn ad essa associate (contatore facilmente calcolabile tramite le informazioni rese disponibili dallo step precedente) e cercando tutte le relazioni in grado di superare la soglia suddetta.
Se almeno una relazione soddisfa questo **vincolo di superamento della soglia**, potremmo allora affermare che questa è la relazione che con estrema probabilità lega le due colonne e che meglio rappresenta la bicolumn considerata. 

Una volta completata questa ricerca, sarà possibile procedere ad:

- **etichettare ciascuna relazione trovata con la coppia di header della bicolumn**;
- **espandere DBpedia con nuovi fatti**, inserendo per ogni relazione una nuova tripla per ciascuna riga che durante lo Step1 non era risultata legata da tale relazione, probabilmente a causa dell'incompletezza del KG. **_Questi saranno i primi fatti estratti da WiKiTEF_**.

_Alla fine dello Step2 per alcune bicolumn il grafo ci permetterà di indentificare la relazione che meglio lega le sue colonne, mentre per altre no_. Per questo motivo si è deciso di marcare (o etichettare) ciascuna bicolumn con la relazione per essa scelta, in modo tale da poter facilmente distinguere le **bicolumn _etichettate_** da quelle **_non etichettate_** che dovranno essere riprocessate nello step successivo.


Facciamo ora un esempio pratico che permetta di cogliere appieno l'approccio utilizzato in WikiTEF per l'estrazione di nuovi fatti dalle tabelle e che permetta allo stesso tempo di esemplificare il funzionamento degli Step 1 e 2. 

**Esempio:** Riprendiamo a tale scopo il precedente esempio relativo alla tabella dei vincitori del premio Paganini, dalla quale abbiamo ottenuto la sola Bicolumn in figura:

<div align="center">
<img src="images/bicolumn.png" alt="BiColumn" width="400" />
</div>

Partendo dalla prima riga, si cerca se nel grafo esiste una relazione da **György_Pauk** a **Hungary** (i wikid delle due entità nella bicolumn e in DBpedia) oppure una relazione da **Hungary** a **György_Pauk**. Supponiamo di trovare la relazione _birthPlace_ che va da György_Pauk a Hungary. Effettuiamo poi tale ricerca per tutte le successive righe della bicolumn. (**Step0**)

Per ogni relazione trovata conteggiamo il numero di righe ad essa associate e supponiamo che il quadro finale sia il seguente:
- l'80% delle righe risultano legate dalla relazione "birthPlace" che va da _Winner_ a _Country_;
- il 15% delle righe risultano legate dalla relazione "deathPlace" che va da _Winner_ a _Country_;
- il 5% delle righe non abbia nessuna relazione associata, poiché nello Step0 non è stato trovato nessun match nel grafo.
poiché la relazione **birthPlace** lega più del 70% delle righe della bicolumn, viene identificata come la relazione da associare a queste coppie di colonne e si memorizzano due informazioni: 

**1.** la coppia di header (Winner, Country) può esprimere la relazione _birthPlace_; 
**2.** la relazione _birthPlace_ è associata alla bicolumn in figura.

A questo punto sarà possibile aggiungere in DBpedia quel 20% delle righe che non sono ancora linkata dalla relazione _birthPlace_, inserendo nel grafo una tripla (wikidEntità1, birthPlace, wikidEntita2) per ciascuna coppia di entità presenti in tali righe.

---
**Schema di sintesi:**
<div align="center">
<img src="images/findRelation_step2.png" alt="Relazione trovata" width="600" />
</div>
<br />
<div align="center">
<img src="images/findRelation_step2_2.png" alt="Conseguenze" width="600" />
</div>

---

Ovviamente nel caso in cui avessimo trovato che la relazione associata alla bicolumn andasse da _Country_ a _Winner_ allora l'avremmo etichettata con gli header (Country, Winner) ed avremmo inserito le triple con le entità inverse (wikidEntità2, _birthPlace_, wikidEntita1)



###2.4 Estrazione di nuovi fatti utilizzando gli header  

Nello Step2 il grafo ci ha permesso di annotare alcune bicolumn, ma non tutte quelle disponibili. Quindi è possibile sfruttare ancora un gran numero di bicolumn non etichettate per tentare di estrarre fatti. _Ma come se il grafo in questo caso non ci aiuta?_
**L'idea è semplice e consiste nell'utilizzare gli _header_ per cercare di estrarre nuovi fatti dalle tabelle o meglio dalle bicolumn**.
Nello step precedente gli header sono stati **annotati** con delle relazioni che li rappresentano, esattamente allo stesso modo in cui in _Lector_ si annotavano le **phrase**. Pertanto così come in _Lector_ si utilizzavano le _phrase_ di una relazione per estrarre nuovi fatti relativi ad essa, allo stesso modo in _WiKiTEF_ è possibile utilizzare gli **header** per:

- _identificare le relazioni da associare ad una bicolumn_;
- _estrarre nuovi fatti per tali relazioni_.

Negli step successivi quindi verranno riprocessate mediante l'utilizzo degli header solo ed esclusivamente le bicolumn per le quali il grafo non ci ha permesso di trovare con certezza una relazione che leghi le loro entità.

Prima di procedere con la descrizione di tali step, è necessario distinguere tra _due casistiche ben diverse_, ovvero dividere le bicolumn non ancora etichettate in due **categorie distinte**, in quanto differiscono tra loro sia per il motivo che ha portato al fallimento dello Step2 sia per la modalità con cui verranno processate negli step successivi:

1. Alla prima categoria appartengono quell'insieme di bicolumn, alle cui righe sono state associate delle relazioni durante lo Step1, ma che non sono state etichettate perché nessuna di queste relazioni è stata in grado di superare la soglia posta nello Step2. Tali bicolumn, indicate come **biColumnUT** (bicolumn under Threshold), verranno processate nello **Step3**.

2. La seconda categoria è invece costituita da tutte quelle bicolumn, per le quali lo Step1 non ha potuto individuare nemmeno una relazione, ovvero bicolumn per le quali nessuna riga ha relazioni associate. Tali bicolonne, ovviamente non etichettate poiché non c'erano relazioni tra cui poter scegliere, saranno processate direttamente nello **Step4**, senza passare per lo Step3, e prendono il nome di **biColumnNR** (bicolumn No Relation).



###2.5 Step 3: Estrazione di nuovi fatti utilizzando header e relazioni delle righe

In questo step, come appena anticipato, verranno processate le bicolumn non etichettate, ma che dispongono di un insieme di relazioni associate alle proprie righe che non hanno superato la soglia impostata per lo Step2. In particolare l'**idea** alla base dello step 3 è quella di realizzare **un join tra le relazioni associate alle righe di una bicolumn e quelle associate ai suoi header** al fine di individuare l'insieme delle relazioni con cui etichettarla.

#### Filtraggio delle bicolumn

Prima di fare ciò viene però realizzato un **filtraggio** delle bicolumn in input, mediante l'utilizzo di una nuova soglia propria dello Step3, che prende il nome di **_soglia media_** e che assume un valore fisso pari a **0.3**. In particolare il _criterio_ utilizzato per filtrare le bicolumn è il seguente:

- le bicolumn, le cui relazioni risultano **tutte** associate ad una percentuale di righe inferiore al 30%, vengono "filtrate" e non processate in questo step, ma in quello successivo (Step4), andandosi ad aggiungere alle bicolumn della seconda categoria (vedi Paragrafo 2.4). 
- le bicolumn, invece, per cui esiste **almeno una** relazione associata ad un numero di righe superiore o pari alla soglia media (ma comunque inferiore alla soglia dello step2) vengono invece processate in questo step.

L'utilizzo di questa ulteriore soglia è motivato dalla seguente _**considerazione**_: Se per una bicolumn nessuna delle relazioni ad esse associate riesce a superare la soglia media del 30% probabilmente è perché sono delle relazioni puramente casuali, che non hanno alcun legame reale con la bicolumn e che pertanto non devono essere utilizzate per assegnare un significato ad essa. Nel caso in cui invece almeno una relazione superi la soglia media, abbiamo maggiore fiducia nel fatto che le relazioni sopra la soglia media possano essere scelte come possibili candidate per rappresentare la bicolumn, ricordando però che tali relazioni non avevano superato la soglia dello Step2. Quindi non sarà possibile utilizzarle direttamente per estrarre fatti senza effettuare degli ulteriori controlli come avveniva nello step2.

Una volta terminato il filtraggio, per ciascuna bicolumn non filtrata si cerca di individuare una o più relazioni con cui etichettarla e con le quali poter estrarre nuovi fatti. La modalità di estrazione dei fatti è praticamente identica a quella utilizzata nello step 2. Ciò che cambia è solamente l'_algoritmo utilizzato per selezionare le relazioni rappresentanti la bicolumn_.

#### Scelta delle relazioni
Consideriamo una singola bicolumn. Per etichettarla si eseguono le seguenti operazioni:

1. Si estrae la coppia di header della bicolumn
2. Si estraggono le relazioni corrispondenti a tali header:
	* Se gli header non hanno associata alcuna relazione, allora non sarà possibile etichettare la bicolumn.
	* Se invece gli header hanno associata almeno una relazione, viene calcolato un **join** tra **le relazioni associate alle righe della bicolumn** e quelle associate **agli header**. Le relazioni risultanti da tale join saranno le relazioni _candidate_ a rappresentare la bicolumn.

Prima di poter essere effettivamente utilizzate per etichettare la bicolumn e per estarre nuovi fatti però tali relazioni dovranno superare un **controllo sui tipi**; controllo del tutto assente nel precedente Step 2, ma che risulta necessario al fine di non introdurre in DBpedia dei fatti errati.

#### Controllo sui tipi ed inserimento nuovi fatti

Il controllo dei tipi eseguito su ciascuna relazione candidata ha come **scopo** quello di verificare che la relazione abbia uno schema, i cui tipi siano coerenti con quelli delle entità presenti nelle due colonne, poiché questa coerenza è una **garanzia** del fatto che la relazione pur essendo associata ad una percentuale di righe comprese tra la soglia media e la soglia dello step2 può rappresentare con un'elevata probabilità la bicolumn considerata.

Una volta chiarite le motivazioni di tale controllo, descriviamo ora come venga realizzato su una singola relazione candidata.
Innanzittuto si considerano le righe della bicolumn e le si dividono in due gruppi: da una parte _le righe che risultano già linkate nel grafo da questa relazione_ e dall'altra _le righe non linkate dalla relazione_, quelle che di fatto vengono utilizzate per creare i nuovi fatti (vedi Step2). Il controllo viene fatto realizzato in riferimento a quest'ultime, infatti, ciò che si fa è verificare che i _tipi relativi allo schema della relazione_ siano gli stessi delle _entità relative a tali righe_.

E’ importante a questo punto sottolineare come non sempre i tipi siano disponibili per ogni entità presente nel grafo, questo perché DBpedia non ha una gerarchia completa di tipi. Quindi potrebbero verificarsi dei casi in cui le entità abbiano entrambe un tipo associato, altri in cui il tipo sia presente solo per una delle due e altri ancora in cui ne siano entrambe prive. Pertanto il controllo si limita ovviamente ai soli tipi esistenti. Inoltre **i tipi possono risultare totalmente o parzialmente assenti anche negli schemi delle relazioni**.

In base a quanto appena detto, si possono verificare 3 casi possibili, che vengono gestiti in maniera diversa:

1. la relazione sottoposta al controllo ha i tipi, ma non sono coerenti con quelli delle righe suddette. In questo caso la relazione corrente non può essere utilizzata per etichettare la bicolumn. 
2. la relazione sottoposta al controllo ha i tipi, e sono coerenti con con quelli delle righe suddette. La relazione è aggiunta quindi all'insieme delle relazioni rappresentanti la bicolumn e utilizzata per aggiungere nuovi fatti in DBpedia con le modalità usate nello step2.
3. la relazione sottoposta al controllo **non** ha i tipi.

In quest'ultimo caso la relazione, pur non avendo i tipi, non può subito essere considerata come buona ed utilizzata per etichettare la bicolumn, ma che deve essere sottoposta ad un ulteriore controllo, che prevede di considerare i tipi delle righe già linkate dalla relazione (che di fatto vengono temporaneamente utilizzati come se fossero il suo schema) e di confrontarli con quelli delle righe non linkate. Se questi tipi coincidono allora la relazione è buona e quindi utilizzabile per etichettare la bicolumn e estrarre nuovi fatti, altrimenti dovrà essere scartata.

Vediamo ora un'**esempio** per che esemplifichi quanto appena descritto.
Consideriamo una bicolumn con header (Writer, Book) e con tipi _Person_ e _Work_ per le sue entità. Supponiamo che dopo l'esecuzione degli Step1 e 2 la situazione sia la seguente: alla bicolumn sono associate tre relazioni _writer_, _director_ e _field_, ciascuna delle quali però non riesce a superare la soglia posta al 70%. Pertanto la bicolumn verrà processata nello Step3. Inoltre all'header (Writer, Book), per via dell'esecuzione dello Step 2 su altre bicolumn, sono state associate le relazioni _writer_ e _director_. 
Nello Step3, poiché l'header ha delle relazioni associate si procede al calcolo del join tra i due insiemi di relazioni appena citati, dalle quali ovviamente si ottengono le relazioni _director_ e _writer_. In seguito al controllo sui tipi, poi solamente la relazione _writer_ risulta buona come relazione rappresentante la bicolumn considerata.


---
**Schema di sintesi**
<div align="center">
<img src="images/findRelation_step3.png" alt="Relazione trovata" width="600" />
</div>
<div align="center">
<img src="images/findRelation_step3_2.png" alt="Conseguenze" width="600" />
</div>

---



###2.6 Step 4 : Estrazione di nuovi fatti utilizzando esclusivamente gli header

**è una variante dello step3 senza la presenza di relazioni associate sulle colonne.**

In questo ultimo Step vengono processate le bicolumn non ancora etichettate e senza alcuna relazione associata, pertanto per riuscire a trovare una o più relazioni che possano identificarle si utilizzano **esclusivamente i loro header**, o meglio le relazioni che nello Step2 sono state associate a tali header durante il processamento di altre bicolumn, per le quali lo step 2 ha avuto esiti positivi.

**Scelta delle relazioni.**  Considerando una singola bicolumn tra quelle processate, per identificare una o più relazioni che leghino le sue colonne è necessario eseguire i seguenti passi:

1. estrarre gli header della bicolumn;
2. ottenere la lista di tutte le relazioni associate a tali header;
3. verificare se esiste almeno una relazione tra queste che sia coerente con i tipi delle entità della bicolumn.
_Se esistono una o più relazioni che soddisfano tale vincolo allora è possibile:_
4. etichettare la bicolumn con queste relazioni;
5. aggiungere in DBpedia le coppie di entità della bicolumn che non risultano già linkate dalle relazioni trovate.

Per la _verifica al punto_ 3 sono possibili diversi casi: 

- Alla coppia di header _non è associata alcuna relazione_. In questo caso non sarà possibile trovare una relazione con cui etichettare la bicolumn nè estarre fatti e la bicolumn restarà pertanto non etichetta.
- Alla coppia di header è associata _una sola relazione_. La verifica in questo caso è semplice, poiché è sufficiente verificare che i tipi delle entità delle due colonne coincidano con quelli presenti nello schema della relazione, facendo solamente attenzione a scegliere come soggetto ed oggetto le prime o le seconde entità delle righe in base al verso con cui la relazione era stata marcata nello Step1.
- Alla coppia di header sono associate _più relazioni_. In questo caso, si itera su tutte le relazioni associate alla coppia di header e si verifica per ognuna se il **vincolo sui tipi** è rispettato. Ogni relazione che soddisfa questo vincolo viene poi selezionata ed utilizzata per etichettare la bicolumn e per estarre nuovi fatti da essa, in maniera identica a quanto avveniva nello Step2. 

Il processo, sintetizzato nella Figura sottostante, è quindi molto simile a quello realizzato nello step2, ciò che cambia è semplicemente il metodo utilizzato per la scelta della relazione, che diversamente da quanto avveniva nello step2, fa uso degli header e realizza un **controllo sui tipi**. Infatti, mentre nello step precedente tale controllo non veniva realizzato, stavolta risulta necessario, in quanto _l'header non può essere generico ma deve essere specificato sui tipi delle entità_ al fine di poter essere correttamente disambiguato, così come dimostrato dal seguente **esempio**:

Consideriamo due bicolumn: una con coppia di header _(Writer, Film)_ e una con header _(Writer, Book)_. A queste bicolumn dovranno essere assegnate due relazioni diverse in base ai tipi delle entità. Nel primo caso infatti le entità avranno tipo _MovieDirector_ o _Person_ per la prima colonna e tipo _Film_ per la seconda e la relazione corretta da associare sarà quella di _director_; nel secondo caso invece si avrà il tipo _Writer_ o _Person_ per la prima colonna e il tipo _Work_ per la seconda ed ovviamente la relazione da associare sarà _writer_ e non più _director_. 

---
**Sintesi per lo Step 4 relativamente all'esempio considerato**
<div align="center">
<img src="images/findRelation_step4.png" alt="Relazione trovata" width="600" />
</div>

---

Pertanto **considerare solamente gli header senza alcun controllo sui tipi sarebbe un errore**, poiché in casi come quello nell'esempio, avrebbe portato alla scelta di una relazione errata (in particolare la relazione _director_ per la bicolumn di cui sopra) e quindi all'estrazione di fatti non accurati.




### 3. IMPLEMENTAZIONE

### 3.1 L'ambiente

Il sistema WikiTEF si basa sull'utilizzo del **Framework .NET** per il linguaggio **C#** e del database non relazionale **MongoDB**. La scelta del linguaggio e del database da utilizzare è motivata da molteplici ragioni, che cerchiamo di descrivere qui di seguito.

Il database **mongodb** è stato scelto in quanto ad oggi la stragrande maggioranza dei **_Big Data_** utilizzano mongodb, e pertanto, dovendo noi lavorare su grandi quantità di dati (più di un milione e mezzo di tabelle, le quali hanno portato alla creazione di più di due milione di bicolumn su cui lavorare), mongodb è stata per noi la scelta migliore che ci ha permesso di **ottimizzare la gestione di tali _big data_**. 
In particolare, MongoDB è un DBMS non relazionale, orientato ai documenti, che viene classificato come un database di tipo NoSQL. MongoDB si allontana quindi dalla struttura tradizionale dei database relazionali basata su tabelle, in favore di documenti in stile JSON con schema dinamico (_formato BSON_), rendendo l'integrazione di dati di alcuni tipi di applicazioni più facile e veloce. 
Questo ha costituito per noi un enorme **vantaggio**, in quanto ci ha permesso di inserire direttamente dentro mongodb il dump in formato JSON delle tabelle di Wikipedia, e di gestire questi dati in C# mediante la realizzazione di un mapping tra **Documenti BSON**, con cui mongo rappresenta i dati delle nostre tabelle, e istanze di corrispondenti classi in C#. Inoltre mongodb è un database che garantisce un'elevata agilità e flessibilità, e che offre un potente sistema di _indicizzazione_, che rendendo possibile la creazione di indici su qualunque campo all'interno di un documento, ci ha permesso di **ottimizzare le nostre query** e di renderle più veloci.

Il **linguaggio C#**, un linguaggio di programmazione orientato agli oggetti sviluppato da Microsoft, è stato invece scelto per tre motivi ben precisi:

1. è un linguaggio **nativo**, e per questo risulta molto più _efficiente_ rispetto ad altri linguaggi interpretati o non nativi, rimanendo comunque molto semplice da utilizzare. Di questo linguaggio si dice infatti che sia _«un linguaggio facile come Java, ma potente come il C++»_;
2. è un linguaggio **multipiattaforma**, dato che l'utilizzo dell'ultima versione di _Mono_ (il compilatore C# per ambienti non Windows) permette di compilare C# nativo sia su ambienti MacOS che Unix, rendendo così il sistema WiKiTEF indipendente dalla particolare piattaforma.
3. le specifiche del linguaggio C# sono descritte dallo standard ISO/IEC 23270:2003. Se quindi un giorno Microsoft decidesse di ritirare C# dal mercato, chiunque potrebbe reimplementarlo.
 
Per quanto riguarda la realizzazione degli esperimenti si è poi reso necessario l'affitto di una macchina virtuale con almeno **16 GB di RAM** tramite il _servizio AWS - Amazon Web Services-_, in quanto le macchine a nostra disposizione non disponendo di una tale memoria, non erano in grado di processare la mole di dati prevista dagli esperimenti stessi (questa scelta verrà motivata adegutamente nel successivo paragrafo relativo alle scelte implementative).

### 3.2 Scelte implementative significative

Per quanto riguarda i dettagli implementativi risulta utile descrivere alcune scelte che evidenziano le tecniche di programmazione utilizzate:

- Tutti gli step vengono eseguiti **multithread** e secondo una politica di stream. Ciò viene in pratica implementato mediante l'utilizzo dei **cursori** di mongodb e della libreria **Parallel** nativa di C#.
I cursori ci permettono di iterare su tutti i documenti di una collezione, per questo sono stati utilizzati con lo scopo di recuperare ciascun documento di interesse dal db da passare poi ad un opportuna classe responsabile della sua lavorazione, al termine della quale poi gli output saranno poi salvati nuovamente nel database. Adottando tale strategia è possibile eseguire il codice in maniera veloce e senza un eccessivo utilizzo delle risorse dovuto al load in memory di tutte le informazioni.
Inoltre, grazie alla libreria Parallel è possibile iterare su un cursore in maniera multithread, in modo tale che venga fornito ad ogni thread un singolo documento da processare, e alla fine del lavoro automaticamente procedere all'elaborazione del successivo.
- Nonostante l'indicizzazione dei dati all'interno del database ci abbia permesso di rendere più veloci le query al db, dovendo lavorare su grandi quantità di documenti, l'elaborazione delle bicolumn ha richiesto comunque di realizzare un elevato numero di ricerche, comportando dei tempi di esecuzione dell'ordine dei 12 giorni, ovviamente _inaccettabili_. Proprio per risolvere questo problema abbiamo creato lo Step1, che caricando in **memoria** tutte le triple di DBpedia, tutte le informazioni sui tipi delle entità e delle relazioni, a fronte di un dispendioso utilizzo di memoria, riesce però a garantire che le successive ricerche possano essere realizzate in tempi molto brevi, disponendo dei dati già in memoria. Lo step1, infatti, effettua tutte le query al db necessarie per collegare tipi e relazioni alle varie bicolonne, in modo tale che, negli step successivi non sia necessaria alcuna ricerca nel database, ma sia sufficiente scorrere le bicolonne per ottenere tutte le informazioni richieste dalle operazioni da realizzare. 
Usando questo approccio in memory siamo riusciti a passare da un tempo di **12 giorni** richiesto per effettuare tutte le query al db a poco più di **2 ore** per salvare tutti i dati in memoria. L'unico svantaggio di questo approccio consiste nell'enorme quantità di RAM necessaria allo step1 per poter caricare tutto in memoria, che è stato però aggirato mediante l'utilizzo di una macchina virtuale in fase di esecuzione che disponesse di almeno 16 GB di RAM.


<br />
<br />

## 4. VALUTAZIONE

Per valutare WiKiTEF abbiamo preso in considerazione un dump di **1652771** tabelle del sito inglese di Wikipedia ed abbiamo eseguito ciascuno Step dell'approccio descritto per provare ad estrarre da tali tabelle nuovi fatti non esistenti in DBpedia.
In particolare le **metriche** utilizzate per valutare WiKiTEF sono due:

1. quantità di nuovi fatti inseriti in DBpedia;
2. quantità di bicolumn etichettate con successo.

Lo **scopo** dei nostri esperimenti, infatti, è proprio quello di riuscire a **quantificare il numero di fatti estratti da WiKiTEF (e non presenti in DBpedia)** ed in seconda analisi di calcolare la percentuale di bicolumn che si è riusciti ad etichettare.

### 4.1 Risultati

Gli step 0 ed 1 come sappiamo servono a preparare il terreno di lavoro per gli step successivi. Pertanto sono stati eseguiti una sola volta ciascuno. In particolare, lo Step0 ha richiesto un tempo di esecuzione di un'ora ed ha portato alla creazione di **2091621** bicolumn. Nello Step1 poi il numero di bicolonne è aumentato per un totale di **2091936** bicolonne. Questo aumento si è reso necessario per via della presenza di alcune bicolumn di dimensioni troppo grandi per poter essere gestite in _mongodb_, le quali hanno richiesto una loro suddivisione in due bicolonne di lunghezza dimezzata in modo tale da poter essere gestite. Anche l'esecuzione di questo step ha impiegato un'ora.


#### Esperimento 1: Step2

Lo step 2 invece è stato eseguito più volte al fine di verificare come _variassero le performance del nostro sistema per diversi valori del suo parametro **soglia**_. Abbiamo così eseguito lo step 2 con valori di soglia compresi tra 0.7 e 0.3 ottenendo i risultati sintetizzati nelle tabelle seguenti:

n° Bicolumn totali = 2091936

n° fatti presenti nel dump di DBpedia considerato inizialmente = 15404324 

valore soglia | # bicolumn etichettate | # fatti estratti | # fatti dopo Step2 
------- | ------- | ------- | ------- 
0.7 | 72333 (3.45%)  | 114373 (0.74%) | 15518697 
0.6 | 97319 (4.65%) | 217607 (1.41%) | 15621931 
0.5 | 135726 (6.48%)  | 378657 (2.45%) | 15782981 
0.4 | 167864 (8%)  | 580239 (3.76%) | 15984563 
0.3 | 207288 (9.9%) | 897733 (5.82%) | 16302057 

Tali percentuali rappresentano rispettivamente la percentuale di bicolumn etichettate rispetto al numero totale di bicolumn create nello step0 e la percentuale di incremento di fatti rispetto al dump originario di DBpedia.

##### Grafici di sintesi

Numero di fatti estratti al variare della soglia

<div align="center">
<img src="images/nuovi_fatti.png" alt="nuovi fatti" width="500" />
</div>

Numero di colonne etichettate al variare della soglia

<div align="center">
<img src="images/bicolumnOK.png" alt="bicolumnOK" width="500" />
</div>

Numero di colonne non etichettate (con relazioni sotto la soglia)

<div align="center">
<img src="images/biColumnUT.png" alt="biColumnUT" width="500" />
</div>

Da questi esperimenti abbiamo potuto constatare che riducendo il valore della soglia utilizzata si riesce ad estarre un numero maggiore di fatti e ad etichettare un numero maggiore di bicolumn, a discapito però dell'accuratezza ed introducendo una maggiore probabilità di commettere degli errori. In quanto, considerando come buone relazioni che superano soglie più basse, potremmo commettere l'errore di associare ad una bicolumn una relazione che non la rappresenti in maniera certa e quindi ciò ci porterebbe ad estarre fatti potenzialmente errati.


#### Esperimenti 2 e 3: Step 3 e 4 con soglia al 70% 

A causa di un inconveniente durante l'esecuzione di tali step non è possibile recuperare e valutare i risultati. Ci promettiamo di farlo appena possibile.



## 5. CONCLUSIONI 

A causa di un inconveniente durante l'esecuzione degli esperimenti non è stato possibile avere una visione d'insieme di tutti i risultati, con cui trarre delle conclusioni. Da alcuni test realizzati in locale durante lo sviluppo, abbiamo però appurato che è possibile grazie a WiKiTEF estrarre nuovi fatti dalle tabelle e soprattutto che l'utilizzo degli header delle tabelle risulta di grande aiuto per avere una maggior sicurezza sui dati estratti.



##6. SVILUPPI FUTURI
Tra i possibili sviluppi futuri di WiKiTEF abbiamo identificato:

1. la **possibilità di impostare il valore della _soglia_ utilizzata nello Step2 in maniera inversamente proporzionale alla lunghezza della bicolumn processata**.
L'idea è quella di non avere più un valore costante della soglia per tutte le bicolumn, così come avveniva nei nostri esperimenti, ma di avere un valore della soglia variabile in base alla lunghezza della singola bicolumn che si sta processando. Questa possibilità è motivata da un attento **ragionamento** che ci ha portato alla seguente _considerazione_: 

	- Se una bicolumn ha un numero molto basso di righe una soglia elevata (ad esempio il 70%) risulta necessaria al fine di trovare una relazione certa che possa rappresentarne il significato;
	- Ma se una bicolumn invece ha un numero estremamente elevato di righe, in questo caso l'utilizzo di una soglia troppo alta non ci permette mai di trovare una relazione in quanto dovrebbe essere associata ad una grande quantità di righe. Sarebbe invece opportuno adottare soglie più basse (ad esempio soglie intorno al 20%) le quali, con molta probabilità, risulterebbero sufficienti per individuare una relazione che con certezza possa rappresentare la bicolumn.

2. la **possibilità di rendere variabile la _soglia media_ utilizzata nello Step 3** che risulta per ora costante e pari a 0.3. 

3. la **possibilità di stimare _l'accuratezza_ dei fatti estratti da WiKiTEF** al fine di valutare in maniera più precisa l'efficienza del nostro sistema, in quanto nelle nostre valutazioni ci siamo limitati a _quantificare il numero di fatti estratti dalle tabelle_ di Wikipedia e non presenti in DBpedia, senza considerare se i fatti estratti siano corretti o errati. 
In particolare, **l'accuratezza dei fatti estratti da WiKiTEF** potrebbe essere stimata andando a valutare manualmente un campione random di questi fatti e valutandone poi la _precision_, così come è stato fatto in _Lector_. La valutazione dei fatti deve necessariamente essere realizzata a mano, in quanto non si dispone di un ground truth con cui confrontarsi, ma può essere realizzata facilmente andando a visualizzare ciascuna tabella da valutare all'interno della sua pagina di Wikipedia e cercando di dedurre, anche in base al contenuto testuale della pagina, quali possano essere le relazioni che legano ciascuna coppia di colonne della tabella e confrontandole poi con quelle scelte da WiKiTEF in modo da stabilire se i fatti aggiunti in DBpedia per quella relazione siano corretti o meno.

4. **possibilità di estendere il controllo dei tipi anche ai supertipi delle entità**, considerando la gerarchia dei tipi di DBpedia. Così facendo, se un'entità ha come tipo _Writer_ e la relazione analizzata ha come tipo soggetto (o oggetto) il tipo _Person_, poiché _Writer_ è un sottotipo di _Person_, allora il controllo sui tipi sarà positivo e l'entità potrà essere parte di una nuova tripla rivestendo il ruolo di soggetto (o oggetto). Questo ci permetterebbe di aumentare ulteriormente il numero di fatti estratti da WiKiTEF, poiché senza controllo sui supertipi, in un caso come quello appena descritto, il controllo darebbe esito negativo e la relazione sarebbe scartata, senza la possibilità di estrarre i fatti corrispondenti.

5. **possibilità di assegnare dei tipi alle relazioni che ne sono prive**, basandosi sui tipi delle entità contenute nelle bicolumn che nello step2 abbiamo etichettato con tali relazioni. Così facendo potremmo sfruttare tali tipi negli step successivi per fare analisi più accurate.
