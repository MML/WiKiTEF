File tables.json = 34.7GB
File tables2.json = 34.7GB
File Infobox = 1.85GB
File InfoboxU = 1.38GB

Database mongo pulito con solo tabelle.json = 6.11GB


Load Infobox start 20:39:54 end 20:43:57

-----------------
Step0 iniziato: 11:47:37 finito: 12:52:45
Importate tutte le bicolumn con count >1
count bicolumns = 4291586
-----------------
Step0 iniziato: 15:03:41 finito: 16:03:21
Importate tutte le bicolumn con count >3
count bicolumns = 2091621
-------------------
Step1 iniziato: 22:27:11 finito: 00:39:13
Aggiunt alle bicolonne tutte le informazioni necessarie, sono diventate 2091936 a causa della duplicazione di alcune troppo grandi per mongodb
------------------
Step2 iniziato: 22:41:25 finito: 00:51:47
SogliaSure: 0.7
Fatti aggiunti: 114K
Fatti aggiunti in relazione a DBPedia: 0.73%
------------------
Step2 iniziato: 20:40:04 finito: 23:25:51
SogliaSure: 0.6
Fatti aggiunti: 122491
Fatti aggiunti in relazione a DBPedia: 0.78%
------------------
Step2 iniziato: 17:59:40 finito: 21:09:33
SogliaSure: 0.5
Fatti aggiunti: 
Fatti aggiunti in relazione a DBPedia: 
------------------
Step2 iniziato: 21:09:33 finito: 03:02:09
SogliaSure: 0.4
Fatti aggiunti: 
Fatti aggiunti in relazione a DBPedia: 
------------------
Step2 iniziato: 03:02:09 finito: 11:47:45
SogliaSure: 0.3
Fatti aggiunti: 
Fatti aggiunti in relazione a DBPedia: 
------------------
Risultati dopo lo Step2

db.getCollection('biColumnsNR').count()										|	1603377	
db.getCollection('biColumnsUT').count({'Threshold' : 70})					|	429258
db.getCollection('biColumnsUT').count({'Threshold' : 60})					|	408759
db.getCollection('biColumnsUT').count({'Threshold' : 50})					|	379084
db.getCollection('biColumnsUT').count({'Threshold' : 40})					|	355722
db.getCollection('biColumnsUT').count({'Threshold' : 30})					|	328803

db.getCollection('headers').count()											|	680530
db.getCollection('headers').count({'Threshold' : 70})						|	72333
db.getCollection('headers').count({'Threshold' : 60})						|	97319
db.getCollection('headers').count({'Threshold' : 50})						|	135726
db.getCollection('headers').count({'Threshold' : 40})						|	167864
db.getCollection('headers').count({'Threshold' : 30})						|	207288

db.getCollection('headersGrouped').count()									|	9568
db.getCollection('headersGrouped').count({'_id.Threshold' : 70})			|	1114
db.getCollection('headersGrouped').count({'_id.Threshold' : 60})			|	1448
db.getCollection('headersGrouped').count({'_id.Threshold' : 50})			|	1909
db.getCollection('headersGrouped').count({'_id.Threshold' : 40})			|	2291
db.getCollection('headersGrouped').count({'_id.Threshold' : 30})			|	2806

db.getCollection('infobox').count()											|	16302057
db.getCollection('infobox').count({'percentage' : 70})						|	114373
db.getCollection('infobox').count({'percentage' : 60})						|	103234
db.getCollection('infobox').count({'percentage' : 50})						|	161050
db.getCollection('infobox').count({'percentage' : 40})						|	201582
db.getCollection('infobox').count({'percentage' : 30})						|	317494
------------------
Step3 iniziato: 02:07:25 finito: 4:19:38
SogliaSure: 0.7
Fatti aggiunti: 
Fatti aggiunti in relazione a DBPedia: 
------------------
Risultati dopo lo Step3

db.getCollection('biColumnsNR').count()										|	1932180	
db.getCollection('biColumnsUT').count({'Threshold' : 70})					|	429258

db.getCollection('headers').count()											|	72333
db.getCollection('headers').count({'Threshold' : 70})						|	72333

db.getCollection('headersGrouped').count()									|	1114
db.getCollection('headersGrouped').count({'_id.Threshold' : 70})			|	1114

db.getCollection('infobox').count()											|	15518697
db.getCollection('infobox').count({'percentage' : 70,'addedWithStep' : 3})	|	16782
------------------
Step4 iniziato: 01:30:20 finito: 17:16:51 (portatile)
SogliaSure: 0.7
Fatti aggiunti: 
Fatti aggiunti in relazione a DBPedia: 
------------------
Risultati dopo lo Step4

db.getCollection('biColumnsNR').count()										|	1932180	
db.getCollection('biColumnsUT').count({'Threshold' : 70})					|	429258

db.getCollection('infobox').count()											|	15548369
db.getCollection('infobox').count({'percentage' : 70,'addedWithStep' : 4})	|	12890
------------------