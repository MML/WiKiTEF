# Backlog

- [x] @kappa89 Correct variable names in Step0.TableParser.Parse()
- [x] @kappa89 Step0.CombinatorialModule.cs Write combinatorial funcion, takes in input a number and returns all couples possibles with numbers from 0 to that number. NO Reverse duplicates. eg 3 => (1,2),(1,3),(2,3)
- [x] @lollo93 Finish method Step0.TableParser.createBiColumns according to comments (iniziato @kappa)
- [x] @lollo93 Add BiColumns facade like TableFacade
- [x] @lollo93 Add to bicolumns facade a method to store a bicolumn and test it!
- [x] @lollo93 Add to bicolumns facade a method to store a list of bicolumns (use method at N4)
- [x] Step 1
- [ ] Step 2
- [ ] Controllare che tutte le chiavi dei vari dictionary abbiano hashcode e equals