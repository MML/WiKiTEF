﻿using System;
using Common.Model;
using Common.Facades;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Step1
{
    public class BiColumnParser
    {
        private BiColumn biColumn;
        private InfoboxFacadeCached infoBoxFacade;
        private BiColumnsNDFacade biColumnsNDFacade;
		private TypeFacade typeFacade;
		private InfoSchemaFacade infoSchemaFacade;

		public BiColumnParser(BiColumn b)
        {
            biColumn = b;
            infoBoxFacade = InfoboxFacadeCached.Instance;
            biColumnsNDFacade = BiColumnsNDFacade.Instance;
			typeFacade = TypeFacade.Instance;
			infoSchemaFacade = InfoSchemaFacade.Instance;
        }

        public void Parse()
        {

            biColumn.Rows.ForEach(x =>
            {
                CreateRelations(x, true);
                CreateRelations(x, false);

				x.Type1 = typeFacade.FindType(x.Title1);
				x.Type2 = typeFacade.FindType(x.Title2);
			});
            biColumnsNDFacade.Add(biColumn);

        }

        private void CreateRelations(Row row, bool Verse)
        {
            List<String> foundRelations;
            if (Verse)
            {
                foundRelations = infoBoxFacade.FindRelations(row.Title1, row.Title2);
            }
            else
            {
                foundRelations = infoBoxFacade.FindRelations(row.Title2, row.Title1);
            }
            if (foundRelations == null)
            {
                return;
            }

            foundRelations.ForEach(s =>
            {
				row.Relations.Add(
					new Relation
					{
						Title = s,
						Verse = Verse,
						TypeObj = infoSchemaFacade.FindObject(s),
						TypeSub = infoSchemaFacade.FindSubject(s)
					}
                );
            });
        }
    }
}
