﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Common.Facades;
using Common.Model;

namespace Step1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Step 1 iniziato alle: " + DateTime.Now.ToString("HH:mm:ss"));

			var infoSchemaFacade = InfoSchemaFacade.Instance;
			var typeFacade = TypeFacade.Instance;
			var infoBoxFacade = InfoboxFacadeCached.Instance;
			Console.WriteLine("Caricati i dati in memory " + DateTime.Now.ToString("HH:mm:ss"));
			infoSchemaFacade = null;
			typeFacade = null;
			infoBoxFacade = null;

            BiColumnsFacade facade = BiColumnsFacade.Instance;

            var cursor = facade.GetCursor();

            /*foreach(BiColumn b in cursor.ToEnumerable())
            {
                BiColumnParser parser = new BiColumnParser(b);
                parser.Parse();
            }
			*/
            Parallel.ForEach(cursor.ToEnumerable(), b =>
            {
                BiColumnParser parser = new BiColumnParser(b);
                parser.Parse();
            });

            Console.WriteLine("Step 1 finito alle: " + DateTime.Now.ToString("HH:mm:ss"));
            Console.ReadLine();
        }
    }
}
