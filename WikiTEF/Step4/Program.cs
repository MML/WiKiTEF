﻿using Common.Facades;
using Common.Model;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Step4
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Step 4 iniziato alle: " + DateTime.Now.ToString("HH:mm:ss"));
			BiColumnsFacadeNoRelation biColumnFacadeNR = BiColumnsFacadeNoRelation.Instance;

			var cursor = biColumnFacadeNR.GetCursor(0.7);

			Parallel.ForEach(cursor.ToEnumerable(), b =>
			{
				BiColumnNRCounter parser = new BiColumnNRCounter(b, 0.7);
				parser.Parse();
			});

			/*foreach (BiColumn b in cursor.ToEnumerable())
			{
				BiColumnNRCounter parser = new BiColumnNRCounter(b, 0.7);
				parser.Parse();
			}*/
			Console.WriteLine("Step 4 finito alle: " + DateTime.Now.ToString("HH:mm:ss"));
			Console.ReadLine();
		}
	}
}
