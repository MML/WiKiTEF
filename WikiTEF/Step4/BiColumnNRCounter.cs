﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Facades;
using Common.Model;
using MongoDB.Driver;

namespace Step4
{
	class BiColumnNRCounter
	{
		private HeadersGroupedFacade headerGroupedFacade;
		private double Threshold;
		private static readonly String thing = "http://www.w3.org/2002/07/owl#Thing";
		private List<Triple> ToBeInserted;
		private InfoboxFacade infoFacade;

		private BiColumn BiColumn;

		public BiColumnNRCounter(BiColumn bicolumn, double threshold)
		{
			headerGroupedFacade = HeadersGroupedFacade.Instance;
			BiColumn = bicolumn;
			Threshold = threshold;
			infoFacade = InfoboxFacade.Instance;
		}

		public void Parse()
		{
			ToBeInserted = new List<Triple>();
			List<HeaderGrouped> headersGrouped = headerGroupedFacade.FindHeaderOrdered(BiColumn.Header1, BiColumn.Header2, Convert.ToInt32(Threshold * 100));
			if (headersGrouped.Count == 0)
			{
				return;
			}
			headersGrouped.ForEach(header =>
			{
				BiColumn.Rows.ForEach(row =>
				{
					if ((header.Header.Header1 == null || header.Header.TypeObj == thing) && (header.Header.Header2 == null) || header.Header.TypeSub == thing)
					{
						return;
					}
					else
					{/** casi in cui si possono effettuare controlli e aggiungere i fatti. (y: presente, n: null)
						T1		T2		TO		 TS
						y	    n		 y		 n
						n		y		 n		 y
						y		y		 y		 y

						*/
						//TODO fai tutti i controlli sugli tipi prima di inserire (controllo null)
						if (row.Type1 != null)
						{
							if (header.Header.TypeObj != null)
							{
								if (row.Type2 != null && header.Header.TypeSub != null)
								{
									if (row.Type1 == header.Header.TypeSub && row.Type2 == header.Header.TypeObj)
									{
										ToBeInserted.Add(Triple.Row2Triple(row, header.Header.Relation, 4, Convert.ToInt32(Threshold * 100), true));
									}
								}
								else
								{
									if (row.Type1 == header.Header.TypeSub)
									{
										ToBeInserted.Add(Triple.Row2Triple(row, header.Header.Relation, 4, Convert.ToInt32(Threshold * 100), true));
									}
								}
							}
						}
						else
						{
							if (row.Type2 != null && header.Header.TypeSub != null)
							{
								if (row.Type2 == header.Header.TypeObj)
								{
									ToBeInserted.Add(Triple.Row2Triple(row, header.Header.Relation, 4, Convert.ToInt32(Threshold * 100), true));
								}
							}

						}
					}

				});
			});
			
			headersGrouped = headerGroupedFacade.FindHeaderOrdered(BiColumn.Header2, BiColumn.Header1, Convert.ToInt32(Threshold * 100)); //inverto gli header e il verso
			if (headersGrouped.Count == 0)
			{
				return;
			}

			headersGrouped.ForEach(header =>
			{
				BiColumn.Rows.ForEach(row =>
				{
					if ((header.Header.Header1 == null || header.Header.TypeObj == thing) && (header.Header.Header2 == null) || header.Header.TypeSub == thing)
					{
						return;
					}
					else
					{
						if (row.Type2 != null)
						{
							if (header.Header.TypeObj != null)
							{
								if (row.Type1 != null && header.Header.TypeSub != null)
								{
									if (row.Type2 == header.Header.TypeSub && row.Type1 == header.Header.TypeObj)
									{
										ToBeInserted.Add(Triple.Row2Triple(row, header.Header.Relation, 4, Convert.ToInt32(Threshold * 100), false));
									}
								}
								else
								{
									if (row.Type2 == header.Header.TypeSub)
									{
										ToBeInserted.Add(Triple.Row2Triple(row, header.Header.Relation, 4, Convert.ToInt32(Threshold * 100), false));
									}
								}
							}
						}
						else
						{
							if (row.Type1 != null && header.Header.TypeSub != null)
							{
								if (row.Type1 == header.Header.TypeObj)
								{
									ToBeInserted.Add(Triple.Row2Triple(row, header.Header.Relation, 4, Convert.ToInt32(Threshold * 100), false));
								}
							}

						}
					}

				});
			});

			if (ToBeInserted.Count > 0)
			{
				try
				{
					infoFacade.AddAll(ToBeInserted);
				}
				catch (MongoBulkWriteException e)
				{
					//This is to permit duplicate key exception to be catched
				}
			}
		}

	}
}
