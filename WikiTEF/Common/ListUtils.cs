﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public static class ListUtils
    {
        public static List<T>[] DivideList<T>(List<T> locations)
        {
            List<T>[] array = new List<T>[2];

            array[0] = locations.Take(locations.Count/2).ToList();
            array[1] = locations.Skip(locations.Count / 2).ToList();

            return array;
        }

    }
}
