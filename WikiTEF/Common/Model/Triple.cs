﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Model
{
    public class Triple
    {
		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public ObjectId Id { get; set; }

		[BsonElement("Relation")]
        [BsonRepresentation(BsonType.String)]
        public string Relation { get; set; }

        [BsonElement("Entity1")]
        [BsonRepresentation(BsonType.String)]
        public string Entity1 { get; set; }

        [BsonElement("Entity2")]
        [BsonRepresentation(BsonType.String)]
        public string Entity2 { get; set; }

        [BsonElement("addedWithStep")]
        [BsonRepresentation(BsonType.Int32)]
        public int AddedWithStep { get; set; }

        [BsonElement("percentage")]
        [BsonRepresentation(BsonType.Int32)]
        public int Percentage { get; set; }


        public static Triple Row2Triple(Row row, String relation, int addedWithStep, int percentage, bool relationVerse)
        {
            if (relationVerse)
            {
                return new Triple
                {
                    Entity1 = row.Title1,
                    Entity2 = row.Title2,
                    Relation = relation,
                    AddedWithStep = addedWithStep,
                    Percentage = percentage
                };
            }
            else
            {
                return new Triple
                {
                    Entity1 = row.Title2,
                    Entity2 = row.Title1,
                    Relation = relation,
                    AddedWithStep = addedWithStep,
                    Percentage = percentage
                };
            }
        }
    }
}
