﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Model
{
    public class Header
    {
		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public ObjectId Id { get; set; }

		[BsonElement("Header1")]
        [BsonRepresentation(BsonType.String)]
        public String Header1 { get; set; }

        [BsonElement("Header2")]
        [BsonRepresentation(BsonType.String)]
        public String Header2 { get; set; }

        [BsonElement("Relation")]
        [BsonRepresentation(BsonType.String)]
        public String Relation { get; set; }

		[BsonElement("TypeObj")]
		[BsonRepresentation(BsonType.String)]
		public String TypeObj { get; set; }

		[BsonElement("TypeSub")]
		[BsonRepresentation(BsonType.String)]
		public String TypeSub { get; set; }

		[BsonElement("Threshold")]
		[BsonRepresentation(BsonType.Int32)]
		public int Threshold { get; set; }
	}
}
