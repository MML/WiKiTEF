﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

namespace Common.Model
{
	public class Row
	{
		[BsonRepresentation(BsonType.Int64)]
		[BsonElement("Id1")]
		public long Id1 { get; set; }

		[BsonRepresentation(BsonType.Int64)]
		[BsonElement("Id2")]
		public long Id2 { get; set; }

		[BsonElement("Title1")]
		[BsonRepresentation(BsonType.String)]
		public String Title1 { get; set; }

		[BsonElement("Title2")]
		[BsonRepresentation(BsonType.String)]
		public String Title2 { get; set; }

		[BsonElement("Exists1")]
		[BsonRepresentation(BsonType.Boolean)]
		public bool Exists1 { get; set; }			//linkType: true INTERNAL, false INTERNAL_RED

		[BsonElement("Exists2")]
		[BsonRepresentation(BsonType.Boolean)]
		public bool Exists2 { get; set; }           //linkType: true INTERNAL, false INTERNAL_RED

		[BsonElement("Type1")]
		[BsonRepresentation(BsonType.String)]
		public String Type1 { get; set; }

		[BsonElement("Type2")]
		[BsonRepresentation(BsonType.String)]
		public String Type2 { get; set; }

		public List<Relation> Relations { get; set; }

		[BsonIgnore]
        private static readonly String DBpediaPrefix = "http://dbpedia.org/resource/";

        public Row(SurfaceLink l1, SurfaceLink l2)
        {
            Id1 = Int64.Parse(l1.Id);
            Id2 = Int64.Parse(l2.Id);

            Title1 = DBpediaPrefix + l1.Title;
            Title2 = DBpediaPrefix + l2.Title;

            Exists1 = l1.LinkType;
            Exists2 = l2.LinkType;
			Relations = new List<Relation>();
        }

		public Row()
		{
			Relations = new List<Relation>();
		}

		public override string ToString()
		{
            return "Title1: " + Title1 + " Title2: " + Title2;
		}

	}
}
