﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Common.Model
{
	public class InfoSchema
	{
		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public ObjectId Id { get; set; }

		[BsonElement("Ontology")]
		[BsonRepresentation(BsonType.String)]
		public String ontology { get; set; }

		[BsonElement("Relation")]
		[BsonRepresentation(BsonType.String)]
		public String relation { get; set; }

		[BsonElement("Data")]
		[BsonRepresentation(BsonType.String)]
		public String data { get; set; }

		public override bool Equals(object obj)
		{
			InfoSchema infoSchema = (InfoSchema) obj;
			return this.ontology.Equals(infoSchema.ontology) && this.relation.Equals(infoSchema.relation) && this.data.Equals(infoSchema.data);
		}

		public override int GetHashCode()
		{
			return this.ontology.GetHashCode() + this.relation.GetHashCode() + this.data.GetHashCode();
		}
	}
}
