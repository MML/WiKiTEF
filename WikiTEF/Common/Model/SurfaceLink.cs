﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Model
{
    public class SurfaceLink
    {
        public bool LinkType { get; set; }
        public String Id { get; set; }
        public String Title { get; set; }

		public override int GetHashCode()
		{
			return Id.GetHashCode() + LinkType.GetHashCode() + Title.GetHashCode();
		}

        public override bool Equals(object obj)
        {
            SurfaceLink other = (SurfaceLink)obj;
            return LinkType == other.LinkType && Id == other.Id && Title == other.Title;
        }
    }
}
