﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Model
{
    public class Table
    {
        public List<SurfaceLink>[,] Rows { get; set; }
        public ObjectId ParentID { get; set; }

        public String[] Headers { get; set; }

        public String PgTitle { get; set; }

        public String SectionTitle { get; set; }

        public String TableCaption { get; set; }
    }
}
