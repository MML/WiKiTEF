﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Model
{
	public class HeaderGrouped
	{
		[BsonElement("_id")]
		public Header Header { get; set;}
		[BsonElement("count")]
		[BsonRepresentation(BsonType.Int32)]
		public int Count { get; set; }
	}
}
