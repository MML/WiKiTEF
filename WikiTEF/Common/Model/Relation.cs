﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace Common.Model
{
	public class Relation
	{
		[BsonElement("Title")]
		[BsonRepresentation(BsonType.String)]
		public String Title { get; set; }

		[BsonElement("Verse")]
		[BsonRepresentation(BsonType.Boolean)]
		public bool Verse { get; set; }     //true: first is subject and second is object; false: otherwise

		[BsonElement("TypeObj")]
		[BsonRepresentation(BsonType.String)]
		public String TypeObj { get; set; }

		[BsonElement("TypeSub")]
		[BsonRepresentation(BsonType.String)]
		public String TypeSub { get; set; }

		public override int GetHashCode()
		{
			return Title.GetHashCode() + Verse.GetHashCode();
		}

		public override bool Equals(object obj)
		{
			Relation other = (Relation)obj;
			return this.Title == other.Title && this.Verse == other.Verse;
		}

		public override string ToString()
		{
			return "Title: " + Title + " Verse: " + Verse;
		}
	}
}
