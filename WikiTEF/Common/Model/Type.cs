﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace Common.Model
{
	public class Type
	{
		[BsonElement("Resource")]
		[BsonRepresentation(BsonType.String)]
		public String Resource { get; set; }

		[BsonElement("InstanceType")]
		[BsonRepresentation(BsonType.String)]
		public String InstanceType { get; set; }

		[BsonElement("Constant")]
		[BsonRepresentation(BsonType.String)]
		public String Constant { get; set; }

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public ObjectId Id { get; set; }

		public override bool Equals(object obj)
		{
			Model.Type type = (Model.Type)obj;
			return this.Resource.Equals(type.Resource) && this.InstanceType.Equals(type.InstanceType) && this.Id.Equals(type.Id);
		}

		public override int GetHashCode()
		{
			return this.Resource.GetHashCode() + this.InstanceType.GetHashCode() + this.Id.GetHashCode(); ;
		}
	}
}
