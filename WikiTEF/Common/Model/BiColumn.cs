﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

namespace Common.Model
{
	public class BiColumn
	{
		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public ObjectId Id { get; set; }

		[BsonElement("ParentID")]
		[BsonRepresentation(BsonType.ObjectId)]
		public ObjectId ParentID { get; set; }

		[BsonElement("PgTitle")]
		[BsonRepresentation(BsonType.String)]
		public String PgTitle { get; set; }

		[BsonElement("SectionTitle")]
		[BsonRepresentation(BsonType.String)]
		public String SectionTitle { get; set; }

		[BsonElement("TableCaption")]
		[BsonRepresentation(BsonType.String)]
		public String TableCaption { get; set; }

		[BsonElement("Rows")]
		public List<Row> Rows { get; set; }

		[BsonElement("Header1")]
		[BsonRepresentation(BsonType.String)]
		public String Header1 { get; set; }

		[BsonElement("Header2")]
		[BsonRepresentation(BsonType.String)]
		public String Header2 { get; set; }

		[BsonElement("Threshold")]
		[BsonRepresentation(BsonType.Int32)]
		public int Threshold { get; set; }

		public override String ToString()
		{
			String rows = "";
			Rows.ForEach(x => {
				rows += x.ToString();
			});
			return "PgTitle: " + PgTitle + ", SectionTitle: " + SectionTitle + ", TableCaption: " + TableCaption + ", Header1: " + Header1 + ", Header2: " + Header2 + "Rows: " + rows;
		}

		public BiColumn()
		{
			Rows = new List<Row>();
		}

		public bool HasRows()
		{
			return Rows.Count != 0;
		}

		public static BiColumn[] splitIn2(BiColumn b)
		{
			var RowsSplitted = ListUtils.DivideList<Row>(b.Rows);
			BiColumn b1 = new BiColumn
			{
				Header1 = b.Header1,
				Header2 = b.Header2,
				ParentID = b.ParentID,
				PgTitle = b.PgTitle,
				SectionTitle = b.SectionTitle,
				TableCaption = b.TableCaption,
				Rows = RowsSplitted[0]
			};
			BiColumn b2 = new BiColumn
			{
				Header1 = b.Header1,
				Header2 = b.Header2,
				ParentID = b.ParentID,
				PgTitle = b.PgTitle,
				SectionTitle = b.SectionTitle,
				TableCaption = b.TableCaption,
				Rows = RowsSplitted[1]
			};
			return new BiColumn[] { b1, b2 };
		}
	}
}
