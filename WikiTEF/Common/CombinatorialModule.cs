﻿using System;
using System.Collections.Generic;

namespace Common
{
    public class CombinatorialModule
    {
        public static List<Tuple<int, int>> GenerateCombinations(int v)
        {
            List<Tuple<int, int>> Listacoppie = new List<Tuple<int, int>>();

            for (int i=0; i<v-1; i++)
            {
                for (int j=i+1; j<v; j++)
                {
                    Listacoppie.Add(new Tuple<int, int>(i, j));
                }
            }
            return Listacoppie;
        }
    }
}