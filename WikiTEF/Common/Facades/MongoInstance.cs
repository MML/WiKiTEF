﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Facades
{
    class MongoInstance
    {
        private static MongoInstance instance;
        private IMongoDatabase database;

        private MongoInstance() {
            MongoClientSettings options = new MongoClientSettings {
                MaxConnectionPoolSize = 800000,
				ConnectTimeout = new TimeSpan(0,10,0),
				WaitQueueSize = 1000,
				WaitQueueTimeout = new TimeSpan(0, 20, 0),
				Server = new MongoServerAddress("localhost")
            };

            MongoClient client = new MongoClient(options);
            database = client.GetDatabase("WikiTEF");
        }

        public static IMongoDatabase Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MongoInstance();
                }
                return instance.database;
            }
        }
    }
}
