﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Model;

namespace Common.Facades
{
	public class TypeFacade
	{
		private static TypeFacade instance;
		private IMongoCollection<Model.Type> collection;
		private Dictionary<String, String> dictionaryTypes;

		private TypeFacade()
		{
			collection = MongoInstance.Instance.GetCollection<Model.Type>("types");

			var filter = Builders<Model.Type>.Filter.Empty;

			dictionaryTypes = new Dictionary<string, string>();

			var types = collection.Find(filter).ToList();
			types.ForEach( x => {
				dictionaryTypes.Add(x.Resource, x.InstanceType);
			});
		}

		public static TypeFacade Instance
		{
			get
			{
				if (instance == null)
				{
					instance = new TypeFacade();
				}
				return instance;
			}
		}

		public void Add(Model.Type elem)
		{
			collection.InsertOne(elem);
		}

		public IAsyncCursor<Model.Type> GetCursor()
		{
			var find = collection.Find(Builders<Model.Type>.Filter.Empty);
			find.Options.NoCursorTimeout = true;
			return find.ToCursor();
		}

		public String FindTypeDB(String Resource)
		{
			var filter = Builders<Model.Type>.Filter.Eq("Resource", Resource);
			var query = collection.Find(filter).SingleOrDefault<Model.Type>();
			if (query != null)
			{
				return query.InstanceType;
			} else
			{
				return null;
			}
		}

		public String FindType(String Resource)
		{
			String result;
			dictionaryTypes.TryGetValue(Resource, out result);
			return result;
		}

		public async Task<String> FindTypeAsync(String Resource)
		{
			var filter = Builders<Model.Type>.Filter.Eq("Resource", Resource);
			var query = await collection.FindAsync(filter);
			var risultato = query.SingleOrDefault<Model.Type>();
			if (risultato != null)
			{
				return risultato.InstanceType;
			}
			else
			{
				return null;
			}
		}
	}
}
