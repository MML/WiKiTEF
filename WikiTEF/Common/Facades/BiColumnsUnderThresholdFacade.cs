﻿using System;
using System.Collections.Generic;
using Common.Model;
using MongoDB.Driver;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Conventions;

namespace Common.Facades
{
    public class BiColumnsUnderThresholdFacade
    {
        private static BiColumnsUnderThresholdFacade instance;
        private IMongoCollection<BiColumn> collection;

        private BiColumnsUnderThresholdFacade()
        {
            collection = MongoInstance.Instance.GetCollection<BiColumn>("biColumnsUT");
			var pack = new ConventionPack();
			pack.Add(new IgnoreIfNullConvention(true));
			ConventionRegistry.Register("ignore nulls",pack,t => true);
		}

		public IAsyncCursor<BiColumn> GetCursor(double threshold)
		{
			var find = collection.Find(Builders<BiColumn>.Filter.Eq("Threshold", Convert.ToInt32(threshold * 100)));
			find.Options.NoCursorTimeout = true;
			return find.ToCursor();
		}

		public static BiColumnsUnderThresholdFacade Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BiColumnsUnderThresholdFacade();
                }
                return instance;
            }
        }

        public void AddAll(List<BiColumn> biColumns)
        {
            collection.InsertMany(biColumns);
        }

		public void Add(BiColumn elem)
		{
			try
			{
				collection.InsertOne(elem);
			}
			catch (System.FormatException e)
			{
				var splitted = BiColumn.splitIn2(elem);
				Add(splitted[0]);
				Add(splitted[1]);
			}
		}

        public IAsyncCursor<BiColumn> getCursor()
        {
			var find = collection.Find(Builders<BiColumn>.Filter.Empty);
			find.Options.NoCursorTimeout = true;
			return find.ToCursor();
		}

		public async Task AddAsync(BiColumn biColumn)
		{
			await collection.InsertOneAsync(biColumn);
		}
	}
}