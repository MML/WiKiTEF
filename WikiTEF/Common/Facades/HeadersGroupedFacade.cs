﻿using Common.Model;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Facades
{
	public class HeadersGroupedFacade
	{
		private static HeadersGroupedFacade instance;
		private IMongoCollection<HeaderGrouped> collection;

		private HeadersGroupedFacade()
		{
			collection = MongoInstance.Instance.GetCollection<HeaderGrouped>("headersGrouped");
		}

		public static HeadersGroupedFacade Instance
		{
			get
			{
				if (instance == null)
				{
					instance = new HeadersGroupedFacade();
				}
				return instance;
			}
		}

		public List<HeaderGrouped> FindHeaderOrdered(String header1, String header2, int threshold)
		{
			var filter = (Builders<HeaderGrouped>.Filter.Eq("_id.Header1", header1) & Builders<HeaderGrouped>.Filter.Eq("_id.Header2", header2) & Builders<HeaderGrouped>.Filter.Eq("_id.Threshold", threshold));
			return collection.Find(filter).ToList();
		}
	}
}
