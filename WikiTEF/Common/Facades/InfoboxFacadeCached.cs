﻿using Common.Model;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common.Facades
{
	public class InfoboxFacadeCached
	{
		private static InfoboxFacadeCached instance;
		private IMongoCollection<Triple> collection;
		private Dictionary<String, List<String>> dictionaryRelations; // key: entita1entita2, value: lista<relazione>

		private InfoboxFacadeCached()
		{
			collection = MongoInstance.Instance.GetCollection<Triple>("infobox");

			var filter = Builders<Triple>.Filter.Empty;
			dictionaryRelations = new Dictionary<string, List<string>>();

			var lista = collection.Find(filter).ToList();
			lista.ForEach(x =>
			{
				String key = x.Entity1 + x.Entity2;

				List<String> result;
				dictionaryRelations.TryGetValue(key, out result);
				if (result != null)
				{
					result.Add(x.Relation);
				}
				else
				{
					dictionaryRelations.Add(key, new List<string>() { x.Relation });
				}
			});

		}

		public static InfoboxFacadeCached Instance
		{
			get
			{
				if (instance == null)
				{
					instance = new InfoboxFacadeCached();
				}
				return instance;
			}
		}

		public void Add(String Entity1, String Entity2, String Relation, int addedWithStep, int percentage) //1 if Step1, 2 if Step2
		{
			Triple document = new Triple
			{
				Entity1 = Entity1,
				Entity2 = Entity2,
				Relation = Relation,
				AddedWithStep = addedWithStep,
				Percentage = percentage
			};
			collection.InsertOne(document);
		}

		public List<String> FindRelations(String entita1, String entita2)
		{
			String key = entita1 + entita2;
			List<String> result = new List<string>();
			dictionaryRelations.TryGetValue(key, out result);
			return result;
		}



		public List<String> FindRelationsDB(String entita1, String entita2)
		{
			var filter = Builders<Triple>.Filter.Eq("Entita1", entita1) & Builders<Triple>.Filter.Eq("Entita2", entita2);

			List<Triple> relations = collection.Find(filter).ToList();
			List<String> stringRelations = new List<string>();
			//brutta cosa, da rivedere.
			foreach (Triple triple in relations)
			{
				stringRelations.Add(triple.Relation);
			}
			return stringRelations;
		}
		public async Task<List<string>> FindRelationsAsync(string entita1, string entita2)
		{
			var filter = Builders<Triple>.Filter.Eq("Entita1", entita1) & Builders<Triple>.Filter.Eq("Entita2", entita2);

			List<Triple> relations = (await collection.FindAsync(filter)).ToList();
			List<String> stringRelations = new List<string>();
			//brutta cosa, da rivedere.
			foreach (Triple triple in relations)
			{
				stringRelations.Add(triple.Relation);
			}
			return stringRelations;
		}

		public void AddAll(List<Triple> rows)
		{
			collection.InsertMany(rows);
		}
	}
}
