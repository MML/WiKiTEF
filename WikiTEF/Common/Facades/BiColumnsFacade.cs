﻿using System;
using System.Collections.Generic;
using Common.Model;
using MongoDB.Driver;
using System.Threading.Tasks;
using System.Linq;

namespace Common.Facades
{
    public class BiColumnsFacade
    {
        private static BiColumnsFacade instance;
        private IMongoCollection<BiColumn> collection;

        private BiColumnsFacade()
        {
            collection = MongoInstance.Instance.GetCollection<BiColumn>("biColumns");
        }

        public static BiColumnsFacade Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BiColumnsFacade();
                }
                return instance;
            }
        }

        public void AddAll(List<BiColumn> biColumns)
        {
            try
            {
                collection.InsertMany(biColumns, new InsertManyOptions {IsOrdered = false });
            } catch (System.FormatException e)
            {
				if (biColumns.Count >= 2)
				{
					List<BiColumn>[] lista = ListUtils.DivideList<BiColumn>(biColumns);
					AddAll(lista[0]);
					AddAll(lista[1]);
				} else
				{
					var splitted = BiColumn.splitIn2(biColumns[0]);
					Add(splitted[0]);
					Add(splitted[1]);
				}
            }
        }

        public void Add(BiColumn elem)
        {
            collection.InsertOne(elem);
        }

        public IAsyncCursor<BiColumn> GetCursor()
        {
			var find = collection.Find(Builders<BiColumn>.Filter.Empty);
			find.Options.NoCursorTimeout = true;
			return find.ToCursor();
        }

    }
}