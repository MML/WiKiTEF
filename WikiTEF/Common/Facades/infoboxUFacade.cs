﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Facades
{
    public class InfoboxUFacade
    {
        private static InfoboxUFacade instance;
        private IMongoCollection<BsonDocument> collection;

        private InfoboxUFacade()
        {
            collection = MongoInstance.Instance.GetCollection<BsonDocument>("infoboxU");
        }

        public static InfoboxUFacade Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new InfoboxUFacade();
                }
                return instance;
            }
        }
        public List<String> FindRelations(String entita1, String entita2)
        {
            var filter = Builders<BsonDocument>.Filter.Eq("Entita1", entita1) & Builders<BsonDocument>.Filter.Eq("Entita2", entita2);
            var projection = Builders<BsonDocument>.Projection.Include("Relazione");

            List<BsonDocument> relations = collection.Find(filter).Project(projection).ToList();
            List<String> stringRelations = new List<string>();
            //brutta cosa, da rivedere.
            foreach (BsonDocument relation in relations)
            {
                stringRelations.Add(relation.ToString());
            }


            return stringRelations;
        }
    }
}
