﻿using System;
using System.Collections.Generic;
using Common.Model;
using MongoDB.Driver;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Conventions;

namespace Common.Facades
{
    public class BiColumnsNDFacade
    {
        private static BiColumnsNDFacade instance;
        private IMongoCollection<BiColumn> collection;

        private BiColumnsNDFacade()
        {
            collection = MongoInstance.Instance.GetCollection<BiColumn>("biColumnsND");
			var pack = new ConventionPack();
			pack.Add(new IgnoreIfNullConvention(true));
			ConventionRegistry.Register("ignore nulls",pack,t => true);
		}

        public static BiColumnsNDFacade Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BiColumnsNDFacade();
                }
                return instance;
            }
        }

        public void AddAll(List<BiColumn> biColumns)
        {
            collection.InsertMany(biColumns);
        }

		public void Add(BiColumn elem)
		{
			try
			{
				collection.InsertOne(elem);
			}
			catch (System.FormatException e)
			{
				var splitted = BiColumn.splitIn2(elem);
				Add(splitted[0]);
				Add(splitted[1]);
			}
		}

        public IAsyncCursor<BiColumn> getCursor()
        {
			var find = collection.Find(Builders<BiColumn>.Filter.Empty);
			find.Options.NoCursorTimeout = true;
			return find.ToCursor();

		}

		public async Task AddAsync(BiColumn biColumn)
		{
			await collection.InsertOneAsync(biColumn);
		}
	}
}