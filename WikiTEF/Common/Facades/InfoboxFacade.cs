﻿using Common.Model;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common.Facades
{
	public class InfoboxFacade
	{
		private static InfoboxFacade instance;
		private IMongoCollection<Triple> collection;

		private InfoboxFacade()
		{
			collection = MongoInstance.Instance.GetCollection<Triple>("infobox");
		}

		public static InfoboxFacade Instance
		{
			get
			{
				if (instance == null)
				{
					instance = new InfoboxFacade();
				}
				return instance;
			}
		}

		public void Add(String Entity1, String Entity2, String Relation, int addedWithStep, int percentage) //1 if Step1, 2 if Step2
		{
			Triple document = new Triple
			{
				Entity1 = Entity1,
				Entity2 = Entity2,
				Relation = Relation,
				AddedWithStep = addedWithStep,
				Percentage = percentage
			};
			collection.InsertOne(document);
		}



		public List<String> FindRelations(String entita1, String entita2)
		{
			var filter = Builders<Triple>.Filter.Eq("Entita1", entita1) & Builders<Triple>.Filter.Eq("Entita2", entita2);

			List<Triple> relations = collection.Find(filter).ToList();
			List<String> stringRelations = new List<string>();
			//brutta cosa, da rivedere.
			foreach (Triple triple in relations)
			{
				stringRelations.Add(triple.Relation);
			}
			return stringRelations;
		}
		public async Task<List<string>> FindRelationsAsync(string entita1, string entita2)
		{
			var filter = Builders<Triple>.Filter.Eq("Entita1", entita1) & Builders<Triple>.Filter.Eq("Entita2", entita2);

			List<Triple> relations = (await collection.FindAsync(filter)).ToList();
			List<String> stringRelations = new List<string>();
			//brutta cosa, da rivedere.
			foreach (Triple triple in relations)
			{
				stringRelations.Add(triple.Relation);
			}
			return stringRelations;
		}

		public void AddAll(List<Triple> rows)
		{
			collection.InsertMany(rows, new InsertManyOptions
			{
				IsOrdered = false
			});
		}
	}
}
