﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Facades
{
    public class TablesFacade
    {
        private static TablesFacade instance;
        private IMongoCollection<BsonDocument> collection;

        private TablesFacade()
        {
            collection = MongoInstance.Instance.GetCollection<BsonDocument>("tables");
        }

        public static TablesFacade Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new TablesFacade();
                }
                return instance;
            }
        }

        public IAsyncCursor<BsonDocument> getCursor()
        {
            ProjectionDefinition<BsonDocument> projection = "{pgTitle: 1, sectionTitle: 1, tableCaption: 1, tableData: 1, tableHeaders: 1}";

            return collection.Find(new BsonDocument()).Project(projection).ToCursor();
        }


    }
}
