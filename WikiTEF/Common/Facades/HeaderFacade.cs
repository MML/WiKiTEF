﻿using Common.Model;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Facades
{
    public class HeaderFacade
    {
        private static HeaderFacade instance;
        private IMongoCollection<Header> collection;

        private HeaderFacade()
        {
            collection = MongoInstance.Instance.GetCollection<Header>("headers");
			var pack = new ConventionPack();
			pack.Add(new IgnoreIfNullConvention(true));
			ConventionRegistry.Register("ignore nulls", pack, t => true);
		}

        public void Add(Header h)
        {
            collection.InsertOne(h);
        }

        public static HeaderFacade Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new HeaderFacade();
                }
                return instance;
            }
        }

		public List<Header> FindHeader(String header1, String header2) //TODO da finire
		{
			var filter = ( Builders<Header>.Filter.Eq("Header1", header1) & Builders<Header>.Filter.Eq("Header2", header2) )
				| ( Builders<Header>.Filter.Eq("Header1", header2) & Builders<Header>.Filter.Eq("Header2", header1) );

			List<Header> headers = collection.Find(filter).ToList();
			return headers;
		}
    }
}
