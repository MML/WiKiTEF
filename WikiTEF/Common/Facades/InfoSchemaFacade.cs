﻿using Common.Model;
using MongoDB.Driver;
using System;
using System.Collections.Generic;

namespace Common.Facades
{
	public class InfoSchemaFacade
	{
		private static InfoSchemaFacade instance;
		private IMongoCollection<InfoSchema> collection;
		private Dictionary<String, String> types2Subject;
		private Dictionary<String, String> types2Object;

		private InfoSchemaFacade()
		{
			collection = MongoInstance.Instance.GetCollection<InfoSchema>("infoSchemas");

			var filterObject = Builders<InfoSchema>.Filter.Eq("Relation", "http://www.w3.org/2000/01/rdf-schema#range");
			var filterSubject = Builders<InfoSchema>.Filter.Eq("Relation", "http://www.w3.org/2000/01/rdf-schema#domain");

			types2Object = new Dictionary<string, string>();
			types2Subject = new Dictionary<string, string>();

			var lista = collection.Find(filterObject).ToList();
			lista.ForEach(x =>
			{
				types2Object.Add(x.ontology, x.data);
			});

			lista = collection.Find(filterSubject).ToList();
			lista.ForEach(x =>
			{
				types2Subject.Add(x.ontology, x.data);
			});
		}

		public static InfoSchemaFacade Instance
		{
			get
			{
				if (instance == null)
				{
					instance = new InfoSchemaFacade();
				}
				return instance;
			}
		}


		public String FindObjectDB(String Ontology)
		{
			var filter = Builders<InfoSchema>.Filter.Eq("Ontology", Ontology) & Builders<InfoSchema>.Filter.Eq("Relation", "http://www.w3.org/2000/01/rdf-schema#range");
			return collection.Find(filter).ToString();
		}
		public String FindSubjectDB(String Ontology)
		{
			var filter = Builders<InfoSchema>.Filter.Eq("Ontology", Ontology) & Builders<InfoSchema>.Filter.Eq("Relation", "http://www.w3.org/2000/01/rdf-schema#domain");
			return collection.Find(filter).ToString();
		}

		public String FindObject(String Ontology)
		{
			String result;
			types2Object.TryGetValue(Ontology, out result);
			return result;
		}
		public String FindSubject(String Ontology)
		{
			String result;
			types2Subject.TryGetValue(Ontology, out result);
			return result;
		}
	}
}
