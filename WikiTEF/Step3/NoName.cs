﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Model;
using Common.Facades;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Step3
{
	class NoName
	{
		private BiColumn biColumn;
		private static readonly double thresholdMediumSure = 0.3;
		private BiColumnsFacadeNoRelation biColumnFacadeNR;
		private HeadersGroupedFacade headerFacade;
		private double ThresholdSure;
		private InfoboxFacade infoFacade;

		public NoName(BiColumn b, double thresholdSure)
		{
			biColumn = b;
			biColumnFacadeNR = BiColumnsFacadeNoRelation.Instance;
			headerFacade = HeadersGroupedFacade.Instance;
			ThresholdSure = thresholdSure;
			infoFacade = InfoboxFacade.Instance;
		}

		internal void Parse()
		{
			Dictionary<Relation, int> numberRelations = new Dictionary<Relation, int>();
			double rowsNumb = biColumn.Rows.Count;
			biColumn.Rows.ForEach(row =>
				{
					row.Relations.ForEach(rel =>
					{
						int result;
						numberRelations.TryGetValue(rel, out result);
						numberRelations[rel] = result + 1;
					});
				});

			var filteredDict = (from entry in numberRelations where (entry.Value / rowsNumb) >= thresholdMediumSure select entry.Key).ToList();

			

			if (filteredDict.Count() > 0)
			{
				//Over medium threshold
				var headers = headerFacade.FindHeaderOrdered(biColumn.Header1, biColumn.Header2, Convert.ToInt32(ThresholdSure * 100));
				var headersFalse = headerFacade.FindHeaderOrdered(biColumn.Header2, biColumn.Header1, Convert.ToInt32(ThresholdSure * 100));
				if (headers.Count == 0 && headersFalse.Count == 0)
				{
					return;
				}
				if (headers.Count > 0)
				{
					FunctionThatWorks(headers, filteredDict, true);
				}

				if (headersFalse.Count > 0)
				{
					FunctionThatWorks(headersFalse, filteredDict, false);
				}
			}
			else
			{
				//Under medium threshold
				biColumn.Id = ObjectId.GenerateNewId();
				biColumnFacadeNR.Add(biColumn);
			}
		}

		private void FunctionThatWorks(List<HeaderGrouped> headers, List<Relation> filteredDict, bool verse) //In italian: Funzione che funziona
		{
			List<Triple> ToBeInserted = new List<Triple>();

			headers.ForEach(h =>
			{
				Relation relFromHead = new Relation { Title = h.Header.Relation, Verse = verse };
				if (filteredDict.Contains(relFromHead)) //Se la relazione la hanno più di thresholdMedium righe
				{
					List<Row> rowsWithoutRelation = new List<Row>();
					List<Row> rowsWithRelation = new List<Row>();
					Dictionary<String, int> commonObjType = new Dictionary<string, int>();
					Dictionary<String, int> commonSubjType = new Dictionary<string, int>();

					biColumn.Rows.ForEach(row =>
					{
						if (!row.Relations.Contains(relFromHead))
						{
							rowsWithoutRelation.Add(row);
						}
						else
						{
							rowsWithRelation.Add(row);
							//Populate dictionaries
							int result;
							if (row.Type1 != null)
							{
								commonObjType.TryGetValue(row.Type1, out result);
								commonObjType[row.Type1] = result + 1;
							}
							if (row.Type2 != null)
							{
								commonSubjType.TryGetValue(row.Type2, out result);
								commonSubjType[row.Type2] = result + 1;
							}
						}
					}
					);
					String mostCommonObj = commonObjType.FirstOrDefault(x => x.Value == commonObjType.Values.Max()).Key;
					String mostCommonSubj = commonSubjType.FirstOrDefault(x => x.Value == commonSubjType.Values.Max()).Key;

					if (!verse)
					{
						String temp = mostCommonObj;
						mostCommonObj = mostCommonSubj;
						mostCommonSubj = temp;

						temp = h.Header.TypeSub;
						h.Header.TypeSub = h.Header.TypeObj;
						h.Header.TypeObj = temp;
					}

					if (h.Header.TypeObj == null && h.Header.TypeSub == null) //Se la relazione non ha tipi
					{
						if (mostCommonObj == null && mostCommonSubj == null) //Se le righe con la relazione non hanno tipi
						{
							return;
						}
						else
						{
							rowsWithoutRelation.ForEach(row =>
							{
								if (mostCommonObj != null)
								{
									if (mostCommonSubj != null)
									{
										if (row.Type2 == mostCommonObj && row.Type1 == mostCommonSubj)
										{
											ToBeInserted.Add(Triple.Row2Triple(row, relFromHead.Title, 3, Convert.ToInt32(ThresholdSure * 100), verse));
										}
									}
									else
									{
										if (row.Type2 == mostCommonObj)
										{
											ToBeInserted.Add(Triple.Row2Triple(row, relFromHead.Title, 3, Convert.ToInt32(ThresholdSure * 100), verse));
										}
									}
								}
								else
								{
									if (mostCommonSubj != null)
									{
										if (row.Type1 == mostCommonSubj)
										{
											ToBeInserted.Add(Triple.Row2Triple(row, relFromHead.Title, 3, Convert.ToInt32(ThresholdSure * 100), verse));
										}
									}
								}
							});
						}
					}
					else //La relazione HA dei tipi, cioè almeno uno dei due NON è null
					{
						rowsWithoutRelation.ForEach(row =>
						{
							if (h.Header.TypeObj != null)
							{
								if (h.Header.TypeSub != null)
								{
									//La relazione ha sia tipo oggetto che tipo soggetto
									if (row.Type2 == h.Header.TypeObj && row.Type1 == h.Header.TypeSub)
									{
										ToBeInserted.Add(Triple.Row2Triple(row, relFromHead.Title, 3, Convert.ToInt32(ThresholdSure * 100), verse));
									}
								}
								else
								{
									//La relazione ha solo l'oggetto
									if (row.Type2 == h.Header.TypeObj)
									{
										ToBeInserted.Add(Triple.Row2Triple(row, relFromHead.Title, 3, Convert.ToInt32(ThresholdSure * 100), verse));
									}
								}
							}
							else
							{
								if (h.Header.TypeSub != null) //Controllo ridondante
								{
									if (row.Type1 == h.Header.TypeSub)
									{
										ToBeInserted.Add(Triple.Row2Triple(row, relFromHead.Title, 3, Convert.ToInt32(ThresholdSure * 100), verse));
									}
								}
							}
						});
					}

				}
			});
			if (ToBeInserted.Count > 0)
			{
				try
				{
					infoFacade.AddAll(ToBeInserted);
				}
				catch (MongoBulkWriteException e)
				{
					//This is to permit duplicate key exception to be catched
				}
			}
		}
	}
}
