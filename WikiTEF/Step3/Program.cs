﻿using Common.Facades;
using Common.Model;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Step3
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Step 3 iniziato alle: " + DateTime.Now.ToString("HH:mm:ss"));

			BiColumnsUnderThresholdFacade facade = BiColumnsUnderThresholdFacade.Instance;

			double threshold = 0.7;

			var cursor = facade.GetCursor(threshold);

			/*foreach (BiColumn b in cursor.ToEnumerable())
			{
				NoName parser = new NoName(b, threshold);
				parser.Parse();
			}
            */
			Parallel.ForEach(cursor.ToEnumerable(), b =>
            {
				NoName parser = new NoName(b, threshold);
				parser.Parse();
            });

			Console.WriteLine("Step 3 finito alle: " + DateTime.Now.ToString("HH:mm:ss"));
			Console.ReadLine();
		}
	}
}
