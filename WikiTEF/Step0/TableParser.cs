﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using Common.Model;
using Common.Facades;
using Common;

namespace Step0
{
    class TableParser
    {
        private BsonDocument Element;
        private BiColumnsFacade facade;

        public TableParser(BsonDocument first)
        {
            this.Element = first;
            this.facade = BiColumnsFacade.Instance;
        }

        internal void Parse()
        {
            BsonArray Headers = Element["tableHeaders"].AsBsonArray;

            foreach (BsonArray header in Headers)
            {
                Table table = CreateTable(Element, header);
                List<Tuple<int, int>> combinations = CombinatorialModule.GenerateCombinations(table.Rows.GetLength(1));
                List<BiColumn> BiColumns = CreateBiColumns(table, combinations);
                table = null; //Force free some memory
                combinations = null;
                if (BiColumns.Count >= 1)
                    facade.AddAll(BiColumns);
                BiColumns = null;
            }
        }

        private List<BiColumn> CreateBiColumns(Table table, List<Tuple<int, int>> combinations)
        {
            List<BiColumn> BiColumns = new List<BiColumn>();
            foreach (Tuple<int, int> tuple in combinations)
            {
                int Col1 = tuple.Item1;
                int Col2 = tuple.Item2;

                BiColumn bicolumn = new BiColumn
                {
                    Header1 = table.Headers[Col1],
                    Header2 = table.Headers[Col2],
                    PgTitle = table.PgTitle,
                    SectionTitle = table.SectionTitle,
                    TableCaption = table.TableCaption,
                    ParentID = table.ParentID
                };

                for (int i = 0; i < table.Rows.GetLength(0) - 1; i++)
                {
                    List<SurfaceLink> surfaceLinks1 = table.Rows[i, Col1];
                    List<SurfaceLink> surfaceLinks2 = table.Rows[i, Col2];

                    if (surfaceLinks1 == null || surfaceLinks1.Count == 0 || surfaceLinks2 == null || surfaceLinks2.Count == 0)
                    {
                        continue;
                    }
                    surfaceLinks1.ForEach(l1 =>
                    {
                        surfaceLinks2.ForEach(l2 =>
                        {
                            bicolumn.Rows.Add(new Row(l1, l2));
                        });
                    });
                }
                if (bicolumn.Rows.Count>3)
                    BiColumns.Add(bicolumn);
            }
            return BiColumns;
        }

        private Table CreateTable(BsonDocument element, BsonArray TableHeader)
        {
            Table table = new Table
            {
                ParentID = element["_id"].AsObjectId,
                PgTitle = element["pgTitle"].ToString(),
                SectionTitle = element["sectionTitle"].ToString()
            };

            BsonValue caption;

            if (element.TryGetValue("tableCaption", out caption))
            {
                table.TableCaption = caption.ToString();
            }


            int totalColumns = 0;

            totalColumns = TableHeader.Count;
            table.Headers = new String[totalColumns];
            for (int i = 0; i < TableHeader.Count; i++)
            {
                table.Headers[i] = TableHeader[i]["text"].ToString();
            }

            int rowsNumber = element["tableData"].AsBsonArray.Count;
            table.Rows = new List<SurfaceLink>[rowsNumber, totalColumns];

            for (int curRow = 0; curRow < rowsNumber; curRow++)
            {
                BsonArray RowA = (BsonArray)element["tableData"].AsBsonArray[curRow];

                for (int curColumn = 0; curColumn < RowA.Count; curColumn++)
                {
                    BsonDocument Cell = (BsonDocument)RowA[curColumn];
                    BsonArray SurfaceLinks = Cell["surfaceLinks"].AsBsonArray;

                    foreach (BsonDocument SurfaceLink in SurfaceLinks)
                    {
                        String LinkType = SurfaceLink["linkType"].ToString();
                        String Id = SurfaceLink["target"]["id"].ToString();
                        String Title = SurfaceLink["target"]["title"].ToString();

                        if (LinkType != "EXTERNAL")
                        {
                            bool exits = LinkType == "INTERNAL";
                            SurfaceLink s = new SurfaceLink
                            {
                                Id = Id,
                                Title = Title,
                                LinkType = exits
                            };

                            if (table.Rows[curRow, curColumn] == null)
                            {
                                table.Rows[curRow, curColumn] = new List<SurfaceLink>
                                {
                                    s
                                };
                            }
                            else
                            {
                                table.Rows[curRow, curColumn].Add(s);
                            }

                        }
                    }

                };
            }
            return table;
        }
    }
}
