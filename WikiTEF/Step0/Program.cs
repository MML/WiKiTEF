﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Model;
using Common.Facades;
using System.Diagnostics;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Step0
{
	class Program
	{
		static void Main(string[] args)
		{

			Console.WriteLine("Step 0 iniziato alle: " + DateTime.Now.ToString("HH:mm:ss"));
			IAsyncCursor<BsonDocument> cursor = TablesFacade.Instance.getCursor();

			Parallel.ForEach(cursor.ToEnumerable(),
			new ParallelOptions { MaxDegreeOfParallelism = 20},
			curObject =>
			{
				curObject = curObject.ToBsonDocument();
				TableParser parser = new TableParser(curObject);
				parser.Parse();
				parser = null; //Force free some memory
			});

			Console.WriteLine("Step 0 finito alle: " + DateTime.Now.ToString("HH:mm:ss"));
			Console.ReadLine();
		}

	}
}
