﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Model;
using Common;
using Common.Facades;

namespace Test
{
    class Program
    {
        void testBiColumn()
        {
            MongoClient client = new MongoClient();
            IMongoDatabase database = client.GetDatabase("WikiTEF");
            IMongoCollection<BiColumn> collection = database.GetCollection<BiColumn>("test");
            //IAsyncCursor<BsonDocument> cursor = collection.Find(new BsonDocument()).ToCursor();


            BiColumn bi;
            bi = new BiColumn
            {
                Header1 = "pippo",
                Header2 = "pluto",
                PgTitle = "ciane",
                SectionTitle = "section test",
                TableCaption = "table test",
                Rows = new List<Row>
                {
                    new Row
                    (
                        new SurfaceLink
                        {
                            LinkType  = true,
                            Id = "1234",
                            Title = "pippo",
                        }
                    ,
                        new SurfaceLink
                        {
                            LinkType  = true,
                            Id = "5234",
                            Title = "ciccio",
                        }
                    )
                }
            };

            collection.InsertOne(bi);
        }

        /*
         *stampa tutte le combinazioni possibile (no reverse eg  se [1,2], no [2,1]
         */
        static void testCombinatorialModule(int v)
        {
            List<Tuple<int, int>> listaCoppie = CombinatorialModule.GenerateCombinations(v);

            if (listaCoppie != null)
            {
                listaCoppie.ForEach(t => { Console.WriteLine("[" + t.Item1 + "," + t.Item2 + "]"); });
            }
            else
            {
                Console.WriteLine("Lista Vuota! 1 solo elemento?");
            }
            Console.WriteLine("Premi invio per uscire");
            Console.ReadLine();
        }

        static void testInfoBox(String entita1, String entita2)
        {
            List<String> relations = InfoboxFacadeCached.Instance.FindRelations(entita1, entita2);
            if (relations.Count < 1)
            {
                Console.WriteLine("lista vuota");
                Console.ReadLine();
            }
            else
            {
                relations.ForEach(x => { Console.WriteLine(x.ToString()); });
                Console.ReadLine();
            }
        }

        static void testInfoBoxU(String entita1, String entita2)
        {
            List<String> relations = InfoboxUFacade.
            Instance.FindRelations(entita1, entita2);
            if (relations.Count < 1)
            {
                Console.WriteLine("lista vuota");
                Console.ReadLine();
            }
            else
            {
                relations.ForEach(x => { Console.WriteLine(x.ToString()); });
                Console.ReadLine();
            }
        }
        private static void testArrayDivision()
        {
            List<int> lista = new List<int> { 1, 2, 3, 4, 5, 6, 7 };
            ListUtils.DivideList(lista);
        }

        static void Main(string[] args)
        {
			//testBiColumn();
			//testCombinatorialModule(2);
			//testInfoBox("http://dbpedia.org/resource/Actrius", "http://dbpedia.org/resource/Ventura_Pons");
			//testInfoBoxU("http://dbpedia.org/resource/Actrius", "http://dbpedia.org/resource/Ventura_Pons");
			//testArrayDivision();
			testMAnyInsertDuplicate();


		}

		private static void testMAnyInsertDuplicate()
		{
			InfoboxFacade a = InfoboxFacade.Instance;
			List<Triple> lista = new List<Triple>
			{
				new Triple
				{
					Entity1 = "http://dbpedia.org/resource/Actrius",
					Entity2 = "http://dbpedia.org/resource/Ventura_Pons",
					Relation = "http://dbpedia.org/ontology/director"
				},
				new Triple
				{
					Entity1 = "pippo",
					Entity2 = "pluto",
					Relation = "disney"
				},
				new Triple
				{
					Entity1 = "pippo2",
					Entity2 = "pluto2",
					Relation = "disney2"
				},
				new Triple
				{
					Entity1 = "pippo3",
					Entity2 = "pluto3",
					Relation = "disney3"
				},
				new Triple
				{
					Entity1 = "pippo23",
					Entity2 = "pluto23",
					Relation = "disney23"
				}
			};
			try
			{
				a.AddAll(lista);
			} catch (MongoBulkWriteException e)
			{

			}
		}
    }
}
