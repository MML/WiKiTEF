﻿using Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using Common.Facades;
using MongoDB.Driver;
using MongoDB.Bson;

namespace Step2
{
	class Counter
	{
		private double ThresholdSure;
		private static readonly double thresholdMediumSure = 0.3;

		private BiColumn biColumn;
		private Dictionary<Relation, int> numberRelations;
		private InfoboxFacade infoboxFacade;
		private HeaderFacade headerFacade;
		private BiColumnsUnderThresholdFacade biColumnsUnderThresholdFacade;
		private BiColumnsFacadeNoRelation bicolumnFacadeNR;
		private bool FirstRun;

		public Counter(BiColumn b, double thresholdSure, bool firstRun)
		{
			infoboxFacade = InfoboxFacade.Instance;
			headerFacade = HeaderFacade.Instance;
			biColumnsUnderThresholdFacade = BiColumnsUnderThresholdFacade.Instance;
			bicolumnFacadeNR = BiColumnsFacadeNoRelation.Instance;
			biColumn = b;
			ThresholdSure = thresholdSure;
			FirstRun = firstRun;
			numberRelations = new Dictionary<Relation, int>();
		}

		public void Parse()
		{
			double rowsNumb = biColumn.Rows.Count;
			biColumn.Rows.ForEach(row =>
			{
				row.Relations.ForEach(rel =>
				{
					int result;
					numberRelations.TryGetValue(rel, out result);
					numberRelations[rel] = result + 1;
				});
			});

			if(numberRelations.Count == 0)
			{
				if (FirstRun)
				{
					bicolumnFacadeNR.Add(biColumn);
				}
				return;
			}

			var filteredDict = from entry in numberRelations where (entry.Value / rowsNumb) >= ThresholdSure select entry;

			//counters = null;

			if (filteredDict.Count() > 0)
			{

				foreach (KeyValuePair<Relation, int> relation in filteredDict)
				{
					if (relation.Value != rowsNumb)
					{
						List<Row> rowsWithoutRelation = new List<Row>();
						biColumn.Rows.ForEach(
							row =>
							{
								if (!row.Relations.Contains(relation.Key))
								{
									rowsWithoutRelation.Add(row);
								};
							}
						);
						List<Triple> triples = new List<Triple>();
						foreach (Row row in rowsWithoutRelation)
						{
							triples.Add(Triple.Row2Triple(row, relation.Key.Title, 2, Convert.ToInt32(ThresholdSure * 100), relation.Key.Verse));
						}
						//rowsWithoutRelation = null;
						try
						{
							infoboxFacade.AddAll(triples);
						}
						catch (MongoBulkWriteException e)
						{
							//This is to permit duplicate key exception to be catched
						}
						
					}

					Header h;
					if (relation.Key.Verse)
					{
						h = new Header
						{
							Header1 = biColumn.Header1,
							Header2 = biColumn.Header2,
							Relation = relation.Key.Title,
							TypeObj = relation.Key.TypeObj,
							TypeSub = relation.Key.TypeSub,
							Threshold = Convert.ToInt32(ThresholdSure * 100)
						};
					}
					else
					{
						h = new Header
						{
							Header1 = biColumn.Header2,
							Header2 = biColumn.Header1,
							Relation = relation.Key.Title,
							TypeObj = relation.Key.TypeObj,
							TypeSub = relation.Key.TypeSub,
							Threshold = Convert.ToInt32(ThresholdSure * 100)
						};

					}
					headerFacade.Add(h);
				}
			}
			else
			{
				//With this bicolumn non ho dedotto alcuna relazione
				biColumn.Threshold = Convert.ToInt32(ThresholdSure * 100);
				biColumn.Id = ObjectId.GenerateNewId();
				biColumnsUnderThresholdFacade.Add(biColumn);
			}
		}
	}
}
