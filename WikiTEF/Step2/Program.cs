﻿using Common.Facades;
using MongoDB.Driver;
using System;
using System.Threading.Tasks;

namespace Step2
{
	class Program
	{
		static void Main(string[] args)
		{
			BiColumnsNDFacade facade = BiColumnsNDFacade.Instance;
            Execute(facade, 0.7, true);
            Execute(facade, 0.6, false);
            Execute(facade, 0.5, false);
            Execute(facade, 0.4, false);
            Execute(facade, 0.3, false);
            Console.ReadLine();

        }

        private static void Execute(BiColumnsNDFacade facade, double threshold, bool firstTime)
        {
            Console.WriteLine("Step 2 iniziato alle: " + DateTime.Now.ToString("HH:mm:ss") + " con soglia: " + threshold);

            var cursor = facade.getCursor();

            Parallel.ForEach(cursor.ToEnumerable(), b =>
            {
                Counter counter = new Counter(b, threshold, firstTime);
                counter.Parse();
                counter = null;
            });

            Console.WriteLine("Step 2 finito alle: " + DateTime.Now.ToString("HH:mm:ss"));
        }
	}
}

