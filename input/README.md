# Input files folder

Expected file structure:
- tables.json
- tables2.json (created by the init scripts)
- dbpedia_mapping_based_triples.tsv
- dbpedia_untrusted_infobox_triples.tsv
- instances_types.tsv
- dbpedia_info_schema.tsv