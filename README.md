# WikiTEF

### A project created to extend DBPedia Knoledge Graph by extracting facts from Wikipedia tables

Remember that all the software are both CPU and I/O Bound, so leave alone your computer while executing, it will scale automatically according to your CPU Threads.

To run this software you require at least 16GB of RAM and 100GB of disk space

#### Init guide:

1. Start mongo
2. Go to initScripts directory
3. Run tables2JSON.sh, you will need a bash to run it. (It works even on windows bash)
4. Go to the directory according to your OS
4. Run loadTables
5. Create collection infobox with unique index on all fields
	`db.getCollection('infobox').ensureIndex({'Entity1': 1, 'Entity2': 1, 'Relation': 1}, { unique: true })`
6. Run loadInfobox
7. Close Mongo
8. Run repairMongo according to your OS //This is just to be sure that everything is fine
9. Start mongo
10. Run loadTypes
11. Run inside mongo shell `db.getCollection('types').ensureIndex({'Resource': 1})`
12. Run loadInfoSchema
13. Run inside mongo shell `db.getCollection('infoSchemas').ensureIndex({'Ontology': 1, 'Relation': 1})`
14. Run inside mongo shell `db.getCollection('biColumnsNR').ensureIndex({'Header1': 1, 'Header2': 1}, { unique: false })`

#### Execution guide:

1. Run Step0
2. Run Step1
3. Run Step2
4. Run `db.headers.aggregate([
	{"$group" :
		{_id:{Relation:"$Relation", Header1:"$Header1", Header2: "$Header2", TypeSub: "$TypeSub", TypeObj: "$TypeObj", Threshold: "$Threshold"}, count:{$sum:1}}
	},
        {$match: {"count":{"$gt":2}}},
        {
		$out : "headersGrouped"
	}
])`
5 Run `db.getCollection('headersGrouped').ensureIndex({'_id.Header1': 1, '_id.Header2': 1}, { unique: false })`
6. Run Step3
7. Run Step4